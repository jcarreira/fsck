// get an hierarchy from file
// list of paths to directories

#include <string>
#include <vector>

#include <sys/types.h>
#include <dirent.h>
#include <cassert>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>

using namespace std;

void get_hierarchy_helper(string dir_name, vector<string>& list_dirs) {
    DIR* dir = opendir(dir_name.c_str());

    //printf("dir:%s\n", dir_name.c_str());
    if (!dir) {
        //printf("%d\n", errno);
        //printf("%d %d %d %d %d %d %d\n", EACCES, EBADF, EMFILE, ENFILE, ENOENT, ENOMEM, ENOTDIR);
        return;
    }
    list_dirs.push_back(dir_name);

    dirent* dentry;
    while ((dentry = readdir(dir))) {
        string path = dir_name + "/" + dentry->d_name;
        struct stat st;
        lstat(path.c_str(), &st);
        unsigned int mode = st.st_mode;

//        if (dentry->d_type == DT_DIR) {
          if (S_ISDIR(mode) && !S_ISLNK(mode)) {
            if (string(dentry->d_name) == ".." || string(dentry->d_name) == ".") {
                continue;
            } else {
                get_hierarchy_helper(path, list_dirs);
            }
        } else {
            string file_name = path + " filetype";
            list_dirs.push_back(file_name);
        }
    }
}

vector<string> get_hierarchy(string file_system) {
    vector<string> list_dirs;

    get_hierarchy_helper(file_system, list_dirs);

    return list_dirs;
}
