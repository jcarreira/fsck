#!/usr/bin/perl

use strict;
use warnings;

$ARGV[0] or die "No fsck path";

my $fsck = $ARGV[0];
my @dirs = glob("test*");

for (sort @dirs) {
    my $dir = $_;

    print "Process dir $dir\n";

    /(\d+)$/;
    my $position = $1;

    print "Address: $position \n";

    die unless chdir ($dir);
    `rm msgs -rf`;
    `find * -name messages.txt | xargs cat >> msgs`;
    `../gen_tests_coverage.pl msgs > testcases`;

#    print "Input Address of directory $dir\n";
#    my $address = <stdin>;
#`../run_test_coverage.pl testcases /home/jcar/Downloads/clean_fsckminix/util-linux-ng-2.13.1.1/disk-utils/fsck.minix ../sb $position`;
    `../run_test_coverage.pl testcases $fsck ../sb $position`;
    die unless chdir("../");
}
