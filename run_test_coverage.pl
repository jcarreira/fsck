#!/usr/bin/perl

use strict;
use warnings;

if (scalar @ARGV < 4) {
    print "run_tests tests fsck sb address <test_state>";
    die "Wrong arguments";
}

my $msg  = $ARGV[0];
my $fsck = $ARGV[1];
my $sb   = $ARGV[2];
my $address   = $ARGV[3];
my $test_state = $ARGV[4];
my $name;
my $disk_out;
my $ntest = 0;

@ARGV = ("$msg");

&read_disk(); # read disk to $disk_out

while (<>) {
#    print "Processing line $.\n";
    /(.*)/g;
#    my $state = $1;
#    my $ret1 = $2;
#    my $ret2 = $3;
#    my $ret3 = $4;
    my $test = $1;

#    if ($ret1 != 255 && $ret2 != 255) { next; }
#    if ($state != 996) { next; }

        my @values = &parse_test($test);
        &run_test(@values);
}

sub run_test {
    my @values = @_;
    my $index = $address;
    $ntest++;

    print STDERR "Values: ", scalar(@values), " in address: $address\n";
    print STDERR "@values \n";


    for (@values) {
        my $value = chr(hex($_));
        substr($disk_out, $index, 1) = $value;
        $index++;
    }

    my $new_disk = "sb_out$ntest";

    open FILE, ">$new_disk" or die "Couldnt open file: $!";
    binmode FILE;
    print FILE $disk_out;
    close FILE;

#print STDERR "dd status=noxfer if=$new_disk of=/dev/ram2\n";
    system("cp $new_disk $new_disk"."clean");
#    system("dd status=noxfer if=$new_disk of=/dev/ram2");# 2>/dev/null");
#    print STDERR "$fsck -fa /dev/ram2\n";
    print STDERR "$fsck -fa $new_disk\n";
#    system("$fsck -fa /dev/ram2 >> log");
    system("$fsck -fa $new_disk >> log");
    system("./check_mountability minix $new_disk");
    system("./check_integrity minix_specification.txt $new_disk $new_disk"."clean");
    my $ret_val1 = $? & 128 ? -1 : $? >> 8;
    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n";
#    print STDERR "$fsck -fa /dev/ram2\n";
#    system("$fsck -fa /dev/ram2 >> log");
    print STDERR "$fsck -fa $new_disk\n";
    system("$fsck -fa $new_disk >> log");
    system("./check_mountability minix $new_disk");
    system("./check_integrity minix_specification.txt $new_disk $new_disk"."clean");
    my $ret_val2 = $? >> 8;
    print STDERR "Ret1: $ret_val1  Ret2: $ret_val2\n";
}



sub parse_test {
    my $test = shift;

    die "not defined test" unless defined $test;

    my @ret;
    my @v = split /\s+/, $test;
    open FILE, ">>t_out" or die "Couldn't open file: $!";

    for (@v) {
        /.*(..)/g;
        push @ret, $1;
print FILE " $1 ";
    }
    
    close FILE;
    return @ret;
}

sub read_disk {
    local $/=undef;
    open FILE, $sb or die "Couldn't open file: $!";
    binmode FILE;
    $disk_out = <FILE>;
    close FILE;
}

sub check_mountability() {

}
