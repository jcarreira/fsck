#include <cstdio>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>
#include "s2e.h"

//const char* mknod_str = "mknod /dev/ram5 -m 660 b 1 1";
//const char* dd_str    = "dd if=/dev/zero of=/dev/ram5 bs=4096 count=100";
const char* fsck_str  = "nice -n -19 ./fsck.ext2 -p -f -Z ram_disk";
const char* gdb_str  = "nice -n -19 gdb ./fsck.ext2 < cmds";
const char* cat_str = "echo \"r -y -f -Z ram_disk\" > cmds";
//const char* mount_str = "mount /dev/ram5 /mnt/ext2";
const char* dmesg_str = "dmesg";
const char* free_str  = "free -m";
//const char* nblocks_str = "dd bs=4 seek=257 count=1 if=hex of=/dev/ram5";
const char* getsb_str  = "./s2eget ram_disk";
const char* getpos  = "./s2eget pos.dat";
//const char* copysb_str = "dd if=sb of=/dev/ram5";// bs=1 seek=1024";
const char* getfsck = "./s2eget Z";
const char* mvfsck  = "mv Z fsck.ext2";
const char* date_str = "date -s \"1 DEC 2011 01:00:00\"";

void progress(const char* s) { puts(s); s2e_message(s); }

int main() {
    int ret_value = 0;
    progress("Ext2");

    // get file system checker
    progress(getfsck);
    system(getfsck);
    progress(mvfsck);
    system(mvfsck);

    // get pos.dat
    progress(getpos);
    system(getpos);

    // get superblock
    progress(getsb_str);
    system(getsb_str);

    // create /dev/ram5
    //progress(mknod_str);
    //system(mknod_str);

    // fix date
    progress(date_str);
    system(date_str);

    // set superblock
    //progress(copysb_str);
    //system(copysb_str);

    s2e_enable_forking();

    // call fsck
    progress(fsck_str);
    int ret = system(fsck_str);

    char out[1000];
    ret = WEXITSTATUS(ret);
    sprintf(out, "fsck return value: %d\n", ret);
    s2e_message(out);

    if (ret == 1 || ret == 2 || ret == 4 || ret == 5 || ret == 6) {
        progress("Running fsck again..");
        progress(fsck_str);
        int ret1 = system(fsck_str);
        ret1 = WEXITSTATUS(ret1);
        sprintf(out, "fsck2 return value: %d\n", ret1);
        s2e_message(out);

        if ( (ret == 1 || ret == 2) && ret1 != 0) {
            ret_value = 1;
            s2e_message("ERROR: (ret == 1 || ret == 2) && ret1 != 0");
        } else if ( (ret == 5 || ret == 6) && ret1 != 4) {
            ret_value = 2;
            s2e_message("ERROR: (ret == 5 || ret == 6) && ret1 != 4");
        } else if (ret == 4 && ret1 != 4) {
            ret_value = 3;
            s2e_message("ERROR: ret = 4 && ret1 != 4");
        } else 
            ret_value = 10;
    }

    s2e_disable_forking();
    s2e_kill_state(ret_value, "TERMINATED");

    return 0;
}

