#!/bin/sh

# script to create base file system for testing


mkdir ext2_fs
mount -o loop -O user_xattr  ext2_image ext2_fs

# create dir 'd' in '/'
mkdir ext2_fs/d

# create file to fill with data and set attributes
touch ext2_fs/f
dd if=zeros of=ext2_fs/f bs=1024 count=278
setfattr -n user.teste -v content ext2_fs/f

# create special files (fifo, char dev, block dev)
mknod ext2_fs/fifo_dev p
mknod ext2_fs/char_dev c 1 1
mknod ext2_fs/block_dev b 8 5

#create symlink and hardlink
ln -s ../ ext2_fs/d/symlink
ln ext2_fs/f ext2_fs/d/hardlink

# create d2 inside dir
mkdir ext2_fs/d/d2

# create another file with 2 blocks
#touch ext2_fs/f2
#dd if=zeros of=ext2_fs/f2 bs=1 count=2048
