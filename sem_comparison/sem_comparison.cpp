#include <cstdio>
#include <errno.h>
#include <sys/mount.h>
#include <cassert>
#include <cstring>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <cstdlib>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>


using namespace std;
FILE* flog = 0;

int system2(const char* str) {
//    puts(str);
    fprintf(flog, "%s\n", str);
    return system(str);
}

string next_dir() {
    static int count = 0;
    count++;
    return string("dir_disk");
}

bool equalLink(int a, int b) { return (a == b || (a == 1 && b==2) || (a==2 && b==1)); }
bool equalUid(int a, int b) { return (a == b || (a == 1000 && b==0) || (a==0 && b==1000)); }
bool equalGid(int a, int b) { return (a == b);}// || (a == 1000 && b==0) || (a==0 && b==1000)); }
struct File {
    char* content;
    string name;
    int nlink;
    int uid;
    int gid;
    unsigned int size;
    int mode;
    string linkname;
    unsigned long int hash;
    string fs;

    File(const string& n, int nl, int u, int g, int s, int m, string l, unsigned long int h, char* c, string fsystem) :
        name(n), nlink(nl), uid(u), gid(g), size(s), mode(m), linkname(l), hash(h), content(c), fs(fsystem)
    {}


    bool operator<(const File& a) const {
        if (name != a.name) return name < a.name;
        //if (!equalLink(nlink, a.nlink)) return nlink < a.nlink;
        //if (!equalUid(uid, a.uid)) return uid < a.uid;
        //if (!equalGid(gid, a.gid)) return gid < a.gid;
        //if (!equalSize(size, a.size)) return size < a.size;
        if (mode != a.mode) return mode < a.mode;
        if (linkname != a.linkname) return linkname < a.linkname;
        if (hash != a.hash) return hash < a.hash;
        return true;
    }
    bool operator==(const File& f) const {
        return name == f.name && //equalLink(nlink, f.nlink) &&
            //equalUid(uid, f.uid) && equalGid(gid, f.gid)  && //size == f.size &&
            mode == f.mode && linkname == f.linkname && hash == f.hash;
    }
    void print(FILE* f) const {
        if (f) {
            fprintf(f,"name: %s nlink: %d uid: %d gid: %d size: %d mode: %d linkname: %s hash: %lu fs: %s\n", 
                    name.c_str(), nlink, uid, gid, size, mode, linkname.c_str(), hash, fs.c_str());
        } else {
            printf("name: %s nlink: %d uid: %d gid: %d size: %d mode: %d linkname: %s hash: %lu fs: %s\n", 
                    name.c_str(), nlink, uid, gid, size, mode, linkname.c_str(), hash, fs.c_str());
        }
    }
};

struct Rep {
    vector<File> files;
    string disk;
    int id;

    void add(File f) {
        files.push_back(f);
    }
    unsigned int size() const {
        return files.size();
    }
    void sort() {
        ::sort(files.begin(), files.end());
    }

    bool operator<(const Rep& r) const {
        if (files.size() != r.files.size()) return files.size() < r.files.size();

        for (int i = 0; i < (int)files.size();i++) {
            if (files[i] == r.files[i])continue;
            return files[i] < r.files[i];
        }
        return false;
    }
    
    bool operator==(const Rep& r) const {
        if (files.size() != r.size()) return 0;
//        puts("Reps size equal");
        for (int i = 0; i < (int)files.size();++i) {
            if (!(files[i] == r.files[i])) {
//                printf("File %d different", i); 
                return false;
            }
        }
        return true;
    }
    void print(FILE* f) const {
        puts("Printing rep");
        for (int i = 0; i < (int)files.size(); ++i)
            files[i].print(f);
    }
};

vector<Rep> reps1;
vector<Rep> reps2;

int getSize(string name) {
    int fd = open(name.c_str(), O_RDONLY);
    assert(fd);
    int size = lseek(fd, 0, SEEK_END);
    if (size == (off_t)-1)
        puts(name.c_str());
    assert(size != (off_t)-1);
    close(fd);
    return size;
}
unsigned long calc_hash(char *str) {
    unsigned long hash = 0;
    int c = 0;

    while ((c = *str++))
        hash = c + (hash << 6) + (hash << 16) - hash;
    return hash;
}

void traverse_dir(const string& dir_name, struct Rep& rep, string fs) {
    DIR* dir = opendir(dir_name.c_str());

    if (!dir) {
        return;
    }

    dirent* dentry;
    while ((dentry = readdir(dir))) {
        if (string(dentry->d_name) == ".." || string(dentry->d_name) == ".")
	    continue;
        char *buf = 0;
        string name = dir_name + "/" + dentry->d_name;
        //printf("Traverse. file: %s type: %d\n", name.c_str(), dentry->d_type);
        if (strcmp(dentry->d_name, "lost+found") == 0)
            continue;
        if (strcmp(dentry->d_name, "24") == 0) {
            closedir(dir);
            return;
        }
        struct stat st;
        lstat(name.c_str(), &st);
        int hardl = st.st_nlink;
        int uid   = st.st_uid;
        int gid   = st.st_gid;
        int mode  = st.st_mode;
        int size  = (mode == 16877 ? 0 : st.st_size);
        unsigned long int hash = 0;
        string linkname;
        

        if (dentry->d_type == DT_DIR || mode == 16877) {
            if (string(dentry->d_name) == ".." || string(dentry->d_name) == ".")
                continue;
            traverse_dir(name, rep, fs);
        }
        else if (dentry->d_type == DT_REG || mode ==33188) {
            int size = getSize(name);
            int fd = open(name.c_str(), O_RDONLY);
            assert(fd);
            if(size == 0) {
              hash = 0;
	    } else {
		    buf = new char[size+1];
		    memset(buf,0,size+1);
		    int rd = read(fd, buf, size);
                    memset(buf + rd, 0, size+1 -rd);
		    hash = calc_hash(buf);
		    //delete[] buf;
	    }
            close(fd);
        }
        else if (dentry->d_type == DT_LNK || mode == 41471) {
            char buf[1000];
            readlink(name.c_str(), buf, sizeof(buf));
            linkname = buf;
        }

        File f(name, hardl, uid, gid, size, mode, linkname, hash, buf, fs);
        rep.add(f);
    }
    closedir(dir);
}

void detach_devices();
void umount_m(const char* dev, int retry = 1, int detach = 1) {
    puts("Umounting");

    while (umount(dev) && retry) {
        sleep(1);
        printf("Umounting. errno: %d\n", errno);
    }
    if (detach)
        detach_devices();
}
void detach_devices() {
    char cmd[100];
    system("umount /dev/loop0");
    system("losetup -d /dev/loop0");
}



void mount_m(const char* disk, string dir, string fs) {
    printf("Mounting disk %s\n", disk);
    char cmd[100];

    umount_m(dir.c_str(), 0);
    sprintf(cmd, "mount -o loop %s %s", disk, dir.c_str());
    system2(cmd);
}

void process_field(const char* field, vector<Rep>& reps, string fsck_rec, string fsck_check, string fs) {
    char cmd[1000], disk[100];
    int ret1, ret2;
    int disk_count = 0;
    string dir = next_dir();

    sprintf(cmd, "find %s/%s -name \"sb_out*\" | awk {\'print $NF\'} > out", fs.c_str(), field);
    system2(cmd);
    sprintf(cmd, "mkdir %s_rec", fs.c_str());
    system(cmd);

    FILE* fin = fopen("out", "r");
    assert(fin);
    while (fscanf(fin, "%s", disk) == 1) {
        sprintf(cmd, "cp %s disk", disk);
        system2(cmd);

        int isDiscarded = 0;
        if (fs.compare("reiserfs") == 0) {
            // recover
            sprintf(cmd, "echo \"Yes\" | %s disk &>/dev/null", fsck_rec.c_str());
            ret1 = system2(cmd);
            ret1 = WEXITSTATUS(ret1);
            // check
            sprintf(cmd, "echo \"Yes\" | %s disk &>/dev/null", fsck_check.c_str());
            ret2 = system2(cmd);
            ret2 = WEXITSTATUS(ret2);
            if (ret2 != 0 && ret2 != 1) 
                isDiscarded = 1;
            printf("disk %s  recRet: %d checkRet: %d\n", disk, ret1, ret2);
        } else if (fs.compare("ext4") == 0) {
            // recover
            sprintf(cmd, "%s disk &>/dev/null", fsck_rec.c_str());
            ret1 = system2(cmd);
            ret1 = WEXITSTATUS(ret1);
            // check
            sprintf(cmd, "echo \"y\n\" | %s disk &>/dev/null", fsck_check.c_str());
            ret2 = system2(cmd);
            ret2 = WEXITSTATUS(ret2);
            if (ret2 != 0 && ret2 != 1) 
                isDiscarded = 1;
            printf("disk %s  recRet: %d checkRet: %d\n", disk, ret1, ret2);
        } else assert(0);

        if (isDiscarded){
            assert(flog);
            fprintf(flog, "Disk %s discarded. ret: %d\n", disk, ret2);
            puts("Disk discarded");
            continue;
        }
        disk_count++;

        sprintf(cmd, "cp disk %s_rec/%d", fs.c_str(), disk_count);
        system2(cmd);

        fprintf(flog, "Disk %s mounted. ret: %d\n", disk, ret2);
        Rep r;
        mount_m(disk, dir, fs);
        printf("Traversing dir %s\n", dir.c_str());
        traverse_dir(dir.c_str(), r, fs);
        r.disk = string(disk);
        r.id = disk_count;


        struct stat st;
        lstat(dir.c_str(), &st);
        File f(dir, st.st_nlink, st.st_uid, st.st_gid, st.st_size, st.st_mode, "", 0, "", fs);
        r.add(f);



//        if (r.size() < 50) {
//            sprintf(cmd, "rm %s -rf", disk);
//            system(cmd);
//            continue;
//        }
	reps.push_back(r);
	r.print(flog);
        umount_m(dir.c_str(), 0);
        dir = next_dir();
        sprintf(cmd, "umount %s", dir.c_str());
        system(cmd);
        system("./free.sh");
//        if (disk_count > 1000)
//            break;
    }
}

void compare_reps(vector<Rep>& r1, vector<Rep>& r2) {
puts("cmp1");
    sort(r1.begin(), r1.end());
puts("cmp2");
    sort(r2.begin(), r2.end());
puts("cmp3");
    r1.erase(unique(r1.begin(), r1.end()), r1.end());
puts("cmp4");
    r2.erase(unique(r2.begin(), r2.end()),r2.end());
puts("cmp5");

    printf("Rep1. size: %u\n", r1.size());
    for (int j = 0; j < (int)r1.size();j++) {
        Rep r = reps1[j];
        reps1[j].sort();
        r.sort();
        printf("Rep1 files. size: %d\n", r.files.size());
        for (int i = 0; i < (int)r.files.size();i++) {
            printf("name: %s type:%d\n", r.files[i].name.c_str(), r.files[i].mode);
        }
    }
    printf("Rep2. size: %d\n", r2.size());
    for (int j = 0; j < (int)r2.size();j++) {
        Rep r = reps2[j];
        reps2[j].sort();
        r.sort();
        printf("Rep2 files. size: %d\n", r.files.size());
        for (int i = 0; i < (int)r.files.size();i++) {
            printf("name: %s\n", r.files[i].name.c_str());
        }
    }
    FILE* fout = fopen("error", "w");
    assert(fout);
    for (int i = 0; i < (int)r1.size(); ++i) {
        for (int j = 0; j < (int)r2.size(); ++j) {
            if (r1[i].size() != r2[j].size()) { printf("%s %s Sizes are different", r1[i].disk.c_str(), r2[j].disk.c_str());
                    r1[i].print(fout);
                    fprintf(fout, "\n\n\n");
                    r2[j].print(fout);
                    exit(-1);
            }
            for (int k = 0; k < (int)r1[i].size();++k) {
                if (!(r1[i].files[k] == r2[j].files[k])) {
                    printf("Files %d are different. disk1: %s disk2: %s\n", k, r1[i].disk.c_str(), r2[j].disk.c_str());

                    r1[i].files[k].print(fout);
                    fprintf(fout, "\n\n\n");
                    r2[j].files[k].print(fout);
                    fprintf(fout, "\n\n\n");
                    r1[i].print(fout);
                    fprintf(fout, "\n\n\n");
                    r2[j].print(fout);
                    exit(-1);
                }
            }
            //if (!(r1[i] == r2[j])) {
            //    printf("files different: disk1:%s id1:%d disk2:%s id2:%d\n", r1[i].disk.c_str(), r1[i].id, r2[j].disk.c_str(), r2[j].id);
            //}
        }
    }
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        //puts("sem_comparison fsck1_rec fsck1_check fsck2_rec fsck2_check");
        puts("sem_comparison fsck2_rec fsck2_check");
        assert(0);
    }
    char* fsck2_rec = argv[1];
    char* fsck2_check = argv[2];
    //char* fsck2_rec = argv[3];
    //char* fsck2_check = argv[4];

    FILE* fin = fopen("fields.txt", "r");
    flog = fopen("log.txt", "w");
    assert(fin);
    assert(flog);

    char field1[100], field2[100], cmd[100];
    while (fscanf(fin, "%s %s", field1, field2) == 2) {
printf("Process field rebuild tree");
        process_field(field1, reps1, "fsck.reiserfs --rebuild-tree", "fsck.reiserfs --check", "reiserfs");
printf("Process field rebuild sb");
        process_field(field1, reps1, "fsck.reiserfs --rebuild-sb", "fsck.reiserfs --check", "reiserfs");
printf("Process field fix fixable");
        process_field(field1, reps1, "fsck.reiserfs --fix-fixable", "fsck.reiserfs --check", "reiserfs");
        process_field(field2, reps2, fsck2_rec, fsck2_check, "ext4");

        int old1 = reps1.size(), old2 = reps2.size();
        printf("Reps1size: %d\n", old1);
        printf("Reps2size: %d\n", old2);
        compare_reps(reps1, reps2);

        printf("Reps1size: %d %d\n", reps1.size(), old1);
        printf("Reps2size: %d %d\n", reps2.size(), old2);
        system("umount /dev/loop0");
        system("losetup -d /dev/loop0");
        system(cmd);
        exit(0);
    }
    return 0;
}

