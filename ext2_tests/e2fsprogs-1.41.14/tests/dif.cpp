#include <cstdio>
#include <cstdlib>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>

#define SIZE 5000000

#define min(a,b) ((a)<(b)?(a):(b))

char buf1[SIZE];
char buf2[SIZE];

int main(int argc, char* argv[]) {
    assert(argc == 2);
    char* image = argv[1];
    char cmd[1000];

    fprintf(stderr, "image: %s\n", image);
    //assert(strcmp(image, "test.img") == 0);

    //sprintf(cmd, "rm %s2", image);
    //system(cmd);
    //sprintf(cmd, "cp %s.gz %s2.gz", image);
    //system(cmd);
    //strcat(image, "2");
    //sprintf(cmd, "gunzip %s.gz", image);
    ////puts(cmd);
    //system(cmd);
    //sprintf(cmd, "dd if=%s of=/dev/ram8", image);
    //puts(cmd);
    //system(cmd);
    //puts("fsck.ext2 -yf /dev/ram8 2>/dev/null");
    sprintf(cmd, "cp %s test2.img", image);
    fprintf(stderr, "%s\n", cmd);
    system(cmd);
    //system("cp test.img test2.img");
    sprintf(cmd, "fsck.ext2 -yf %s 2>/dev/null >/dev/null", image);
    fprintf(stderr, "%s\n", cmd);
    system("fsck.ext2 -yf test.img 2>/dev/null >/dev/null");

    //puts("dd if=/dev/ram8 of=image_modified");
    //system("dd if=/dev/ram8 of=image_modified");

    int fin1 = open(image, O_RDONLY);
    int fin2 = open("test2.img", O_RDONLY);
    assert(fin1);
    assert(fin2);

    fprintf(stderr, "Checking difference\n");
    int size1 = read(fin1, buf1, sizeof(buf1));
    int size2 = read(fin2, buf2, sizeof(buf2));

    int m = min(size1, size2);

    for (int i = 0; i < m; ++i) {
        if (buf1[i] != buf2[i]) {
            printf("byte%d\n", i);
            printf("%d\n1\n", i);
        }
    }

    return 0;
}

