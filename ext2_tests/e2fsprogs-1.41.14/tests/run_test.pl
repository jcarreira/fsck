#!/usr/bin/perl

use strict;
use warnings;

my $timeout = 1800;
my $test_dir = $ARGV[0];
my $tmpfile = $ARGV[1];

&process_test($test_dir);

sub process_test {
    my $dir = shift;

    my $FILE;
    open $FILE, ">pos.dat";
    die unless $FILE;

    print "Calculating difs: $dir\n";
    print "cp $tmpfile $test_dir.img\n";
    `cp $tmpfile $test_dir.img`;
    `./dif $tmpfile > pos.dat`;
#@ARGV = ("dif_bytes");

#    while (<>) {
#        print $FILE "byte1";
#        print $FILE "$_\n";
#        print $FILE "1\n";
#    }

    print "cp pos.dat ../../../build_tests/tests/ \n";
    `cp pos.dat ../../../build_tests/tests/`; # bytes to mark as symbolic
    print "cp $test_dir.img ../../../build_tests/tests/ram_disk\n";
    `cp $test_dir.img ../../../build_tests/tests/ram_disk`; # copy disk
    chdir("../../../build_tests/");
    print "Starting S2E\n";

    my $pid = fork();
    if (not defined $pid) {
        die "Error forking";
    } elsif ($pid == 0) { #child
        print "./launchvm.sh ready -s2e-max-processes 8 -nographic\n";
        `./launchvm.sh ready -s2e-max-processes 8 -nographic`;
#        `./launchvm.sh 2`;
    } else {
        eval {
            local $SIG{ALRM} = sub { print "Killing s2e\n"; `pgrep qemu | xargs kill -9` };
            alarm $timeout;
            waitpid($pid,0);
            alarm 0;
        };
    }

    $test_dir =~ s/\.\///g;

    `rm test$test_dir`;
    `find s2e-last/* -name messages.txt | xargs cat >> test$test_dir`;
    `cp test$test_dir ../ext2_tests/e2fsprogs-1.41.14/tests/`;
    chdir("../ext2_tests/e2fsprogs-1.41.14/tests");
}
