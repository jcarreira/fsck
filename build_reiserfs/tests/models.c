#ifndef _MODELS_H_
#define _MODELS_H_

#include <errno.h>
#include <stddef.h>
#include "s2e.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>

static const int BLOCK_SIZE = 1024;

#define BLOCKS 10000
#define FSIZE 300
#define MEM_SIZE 10*1024*1024
#define MAX_SIZE (BLOCKS * 1024)

static int loadFromFile = 0;
const int SYMB_N = 1;
const int SYMB_ADDRESS[] = {5120};
const int SYMB_SIZE[] = {2048};
const char* SYMB_DESC[] = {"symbolic inode table"};

const char* SHMEM_NAME = "sb_shmem";

typedef struct region {
    int size;
    char data[MAX_SIZE];
}region;

typedef struct file {
    char name[40];
    unsigned int seek;
    char* content;
    unsigned int size;
    int mode;
    char written;
    char open;
} file;

static int RUN = 0;
char* mem;
unsigned int off=0;
static int _FD = -1;

file files[FSIZE] = {0};

void assert_model(char b, const char* msg) {
    if (!b) {
        if (s2e_version() != 0) {
            s2e_message("ERROR model: ");
            s2e_kill_state(-2, msg);
        } else {
            puts(msg);
            assert(0);
        }
    }
}
//
//void init_model(int bs, char lff) {
//    s2e_message("Model init");
//    //mem = (char*)malloc(MEM_SIZE);
//    //assert_model(mem != 0, "error in malloc");
//    loadFromFile = lff;
//}
//
////static char* mem;
////
////void memory_init() {
////    mem = (char*)malloc(SIZE);
////}
////
////
////void* malloc(size_t size) {
////    static int ptr = 0;
////
////    if (ptr >= SIZE)
////        s2e_kill_state(-1, "ERROR: Not enough mem");
////
////    ptr += size;
////    return (void*) (mem + ptr - size);
////}
//
//int open_m(const char *filename, int mode, const char* sb_file) {
//    int fd = -1;
//    int rd;
//    int READING_DISK = 0;
//
//    s2e_message("open_m. name:");
//    s2e_message(filename);
//
////    if (!strstr(filename, "sb2"))
////        return open(filename, mode);
//
//    assert_model(filename[0] != 0, "filename invalid");
//    assert_model(BLOCK_SIZE != 0, "BS is zero");
//
//    if (strstr(filename, "sb2") != 0)
//       READING_DISK = 1;
//
//    // check if file is opened
//    int i;
//    for (i = 0; i < FSIZE; i++) {
//        if (files[i].name[0] && strcmp(files[i].name, filename) == 0)
//            break;
//    }
//
////    for (i = 0; files[i].name[0] && strcmp(files[i].name, filename) != 0 && i < FSIZE; ++i)
////        ;
//
//    //assert_model(i < FSIZE, "i < FSIZE");
//
//    if (i < FSIZE) { // this file is already opened. create new fd, share content
//        s2e_message("File already opened");
//        int j;
//        for (j = i; j < FSIZE && files[j].name[0]; ++j) // find empty spot
//            ;
//        j=open(filename, mode);
//        assert_model(j < FSIZE, "j < FSIZE");
//        strcpy(files[j].name, files[i].name);
//        files[j].seek = 0;
//        files[j].mode = mode;
//        files[j].content = files[i].content;
//        files[j].size = files[i].size;
//        s2e_message("fd");
//        s2e_message_int(j);
//        return j;
//    } else { // this file is not open. i is an empty slot. assume we are opening disk
//        fd = i;
//        fd=open(filename, mode);
//        s2e_message("Saving file in position");
//        s2e_message_int(fd);
//
//        strcpy(files[fd].name, filename);
//        files[fd].seek = 0;
//        files[fd].mode = mode;
//    }
//
//    if (READING_DISK == 1) {
//	int shmfd = shm_open(SHMEM_NAME, O_RDONLY, S_IRUSR);
//	if (shmfd == -1 && _FD == -1) { // first run. get superblock from file 'sb'
//		s2e_message_int(errno);
//		s2e_message("First run. Getting sb from file.");
//		RUN = 1;
//		int filed = open(sb_file, mode);
//		s2e_assert(filed != -1, "SB read");
//
//                files[fd].content = (char*)malloc(BLOCKS * BLOCK_SIZE); 
//		rd = read(filed, files[fd].content, BLOCKS * BLOCK_SIZE);
//		files[fd].size = rd;
//		close(filed);
//
//                if (s2e_version() != 0) {
//                    if (loadFromFile) {
//                        s2e_message("Getting sb from pos.dat.");
//                        FILE* fin = fopen("pos.dat", "r");
//                        char description[100];
//                        int pos, length;
//                        s2e_message("P1");
//                        assert(fin);
//                        s2e_message("P2");
//
//                        while (fscanf(fin, "%s %d %d", description, &pos, &length) == 3) {
//                            s2e_message(description);
//                            s2e_message_int(pos);
//                            s2e_message_int(length);
//                            s2e_make_symbolic(files[fd].content+pos, length, description);
//                        }
//                        
//                        fclose(fin);
//                    } else {
//			int i;
//			for (i = 0; i < SYMB_N; ++i) {
//				s2e_make_symbolic(files[fd].content+SYMB_ADDRESS[i], SYMB_SIZE[i], SYMB_DESC[i]);
//			}
//                    }
//		
//		}
//                if (_FD == -1)
//                    _FD = fd;
//                return fd;
//	} else {
//		s2e_message("Accessing shared memory..");
//		RUN = 2;
//		region* ptr = (region*)mmap(NULL, sizeof(struct region),
//			PROT_READ, MAP_SHARED, shmfd, 0);
//                if (ptr == MAP_FAILED) {
//                    s2e_message("mmap error");
//                    s2e_message_int(errno);
//                    assert_model(ptr != 0, "ptr != 0");
//                }
//                files[fd].size = 0;
//                files[fd].content = (char*)malloc(ptr->size);
//                assert_model(files[fd].content != 0, "Not enough content memory");
//                s2e_message("region size:"); s2e_message_int(ptr->size);
//                memcpy(files[fd].content, ptr->data, ptr->size);
//                files[fd].size = rd = ptr->size;
//               
//                shm_unlink(SHMEM_NAME);
//                if (_FD == -1)
//                    _FD = fd;
//		return fd;
//	}
//    } else {
//       assert_model(0, "Not reading disk");
//    }
//
//    if (_FD == -1)
//        _FD = fd;
//    return fd;
//}
//
//void close_m() {
//
//}
//
//off_t lseek_m(int fd, off_t offset, int whence) {
//    assert_model(fd >= 0 && fd < FSIZE, "lseek:fd not in the range");
//    s2e_message_int(whence);
//    assert_model(whence == SEEK_SET || whence == SEEK_CUR || whence == SEEK_END, "whence invalid");
//
//    s2e_message("fd:");
//    s2e_message_int(fd);
//
//    if (whence == SEEK_SET) {
//        s2e_message("SEEK_SET");
//        s2e_message_int(offset);
//        if (offset < 0 || offset > files[fd].size) { errno = EINVAL; return -1; }
//        files[fd].seek = offset;
//    } else if (whence == SEEK_CUR) {
//        s2e_message("SEEK_CUR");
//        s2e_message_int(offset);
//        if (files[fd].seek + offset < 0 || files[fd].seek + offset > files[fd].size) { errno = EINVAL; return -1; }
//        files[fd].seek += offset;
//    } else if (SEEK_END) {
//        s2e_kill_state(-1, "ERROR: SEEK_END not implemented");
//    } else s2e_kill_state(-1, "ERROR: whence not found.");
//
//    //assert_model(files[fd].seek >= 0 && files[fd].seek < files[fd].size, "incorrect seek");
//    return lseek(fd, offset, whence);
//    s2e_message("SEEK is over");
//    return files[fd].seek;
//}
//
//ssize_t read_m(int fd, void *buf, size_t count) {
//    s2e_message("READ_M.name:");
//    s2e_message(files[fd].name);
//    s2e_message_int(fd);
//    if (files[fd].name[0] == 0)
//        return read(fd, buf, count);
//
//    assert_model(fd >= 0 && fd < FSIZE, "read: fd not in the range");
//    assert_model(files[fd].content != 0, "read:file not open");
//
//    s2e_message_int(files[fd].seek);
//    s2e_message_int(count);
//    s2e_message_int(lseek(fd, 0, SEEK_CUR));
//    s2e_message("READ2");
//
//    int ret = count;
//
//    if (files[fd].seek + count > files[fd].size) {
//        count = files[fd].size - files[fd].seek;
//        ret = 0;
//    }
//
//    char *vec = (char*)buf;
//    //s2e_print_expression("files[fd].seek", files[fd].seek);
//    //s2e_print_expression("count", count);
//
//    read(fd, buf, count); // READ before we do it to update the seek pointer
//    s2e_message("READ3");
//    memcpy(vec, &files[fd].content[files[fd].seek], count);
//    s2e_message("READ4");
//    //s2e_message("after memcpy");
//    files[fd].seek += count;
//
//    s2e_message("ret");
//    s2e_message_int(ret);
//    //s2e_message_int(ret2);
//
//    //return ret2;
//    return ret;
//}
//
//ssize_t write_m(int fd, const void *buf, size_t count) {
//    s2e_message("WRITE");
//    assert_model(fd >= 0 && fd < FSIZE, "write: fd not in the range");
//    assert_model(files[fd].content != 0, "write:file not open");
//    assert_model(files[fd].seek + count < files[fd].size, "write: not enough file space");
//
//    s2e_message("WRITE_M");
//    s2e_message_int(files[fd].seek);
//    s2e_message_int(count);
//
//    assert_model(fd == _FD, "fd == _FD");
//    write(fd, buf, count); // write
//    memcpy(files[fd].content + files[fd].seek, (char*)buf, count);
//    files[fd].seek += count;
//    return count;
//}
//
//void* malloc_m(unsigned int n) {
//    assert_model(off + n < MEM_SIZE, "not enough memory");
//    void* ret = &mem[off];
//    off += n;
//    return ret;
//}
//
//static void save_data(const char *file, char* data, int size) {
//    int fd = shm_open(file, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
//    assert_model(fd != -1, "fd != -1");
//    assert_model(size < MAX_SIZE, "size < MAX_SIZE");
//
//    if (ftruncate(fd, sizeof(struct region)) == -1)
//        assert_model(0, "error truncating shmem");
//
//
//    region* r = (region*) mmap(NULL, sizeof(struct region),
//                   PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
//    
//
//    if (r == MAP_FAILED)
//        assert_model(0, "error mmapping");
//
//    r->size = size;
//    s2e_message("Shared memory size: ");
//    s2e_message_int(size);
//    memcpy(r->data, data, size);
//
//    //int i;
//    //for (i = 0; i < size; ++i) {
//    //    s2e_print_expression("data[i]", data[i]);
//    //}
//}

char in[1024*1024];
void end_model(char recovered_path, int retcode) {
    return;
    assert_model(_FD != -1, "_FD != -1");

    int diskfd = _FD;
    file f = files[diskfd];

    // avoid wrapping up more than once (due to atexit)
    static int ended = 0;
    if (ended)
        return;
    else ended++;
    
    //s2e_message("Test data:");
    //s2e_message("retcode:");
    //s2e_message_int(retcode);
//    int j;
//if (RUN != 1) goto skip;
    //goto skip;
    //for (j = 0; j < SYMB_N; ++j) {
    //    s2e_message_int2("symb address:" , SYMB_ADDRESS[j]);
    //    s2e_message_int2("symb size:", SYMB_SIZE[j]);
    //    sprintf(out, "%s:", SYMB_DESC[j]);
    //    int len = strlen(out);
 
    //    int i;
    //    for (i = 0; i < SYMB_SIZE[j]; ++i) {
    //        char byte = f.content[i + SYMB_ADDRESS[j]];
    //        unsigned char byte2 = s2e_get_example_uint(byte);
    //        sprintf(out + len, " %02x ", byte2);
    //        while (out[len])
    //            len++;
    //    }
    //    s2e_message(out);
    //}

//skip:

    //s2e_message("Saving data in shared memory.");
    //if ((retcode == 0 || retcode == 4 || retcode == 8) && f.written == 1) {
    //    s2e_message("ERROR: written to disk.");
    //}

    //if (RUN == 1 && (retcode == 1 || retcode == 2 || retcode == 4 || retcode == 5 || retcode == 6)) { // first run. should save data in shared memory
    //    s2e_message("Saving data in shared memory.");
    //    save_data(SHMEM_NAME, files[_FD].content, files[_FD].size);
    //} else if (RUN == 2 && (retcode == 1 || retcode == 2 || retcode == 5 || retcode == 6)) {
    //    s2e_message("shm_unlink");
    //    shm_unlink(SHMEM_NAME);
    //    //s2e_message("ERROR: fsck recovered second time");
    //    //s2e_message_int(retcode);
    //} else if (RUN == 2) {
    //    s2e_message("shm_unlink2");
    //    shm_unlink(SHMEM_NAME);
    //}
}



#endif

