#!/usr/bin/perl

# First pass gets the return value for the first run of the fsck
# Second pass gets the second run
# Third run gets the status value returned and the test case
# Fourth pass prints the test cases (to stdout)

use strict;
use warnings;

if (scalar @ARGV != 1 && @ARGV != 2) {
    print "generate_tests messages.txt <symbolic_name>";
    die "Wrong arguments";
}

my $msg  = $ARGV[0];
my $name = $ARGV[1];
my $FIRST = 1;
my $SECOND = 2;
my $STATUS = 3;
my $TEST = 4;
my %ret;
my $input;

@ARGV = ("$msg");

{
    local $/=undef;
    open FILE, $msg or die "Couldn't open file: $!";
    binmode FILE;
    $input = <FILE>;
    close FILE;
}

#while (1) {
    print STDERR "name: $name\n" if defined $name;
    if (not defined $name) {
        if($input =~ m/with name \'(.*)\'/) {
            $name = $1;
        }
    }
#}

pos($input) = 0;
print STDERR "Checking 1st\n";
while ($input =~ m/\[State (\d+)\].*fsck return value: (\d+)/g) {
    $ret{$1}{$FIRST} = $2;
    print STDERR "State: $1  ret: $2 \n";
}

pos($input) = 0;
print STDERR "Checking 2nd\n";
while ($input =~ m/\[State (\d+)\].*fsck2 return value: (\d+)/g) {
    warn "SECOND but not FIRST state $1" if not defined $ret{$1}{$FIRST};
    $ret{$1}{$FIRST} = -2 if not defined $ret{$1}{$FIRST};
    $ret{$1}{$SECOND} = $2;
    print STDERR "State: $1  ret2: $2 \n";
}

pos($input) = 0;
print STDERR "Checking 3rd\n";
while ($input =~ /Terminating state (\d+) with (error )?message 'State was terminated by opcode/g) { # get state number
    print STDERR "State $1. name: $name\n";
    my $state = $1;
    die "error matching state $state" unless ($input =~ /status: (\d+)/g); # get return status that fsck.c returned
    print STDERR "Status $1\n";
    my $status = $ret{$state}{$STATUS} = $1;
    die "error getting state: $state testcase" unless ($input =~ /processTestCase of state (\d+)/g);  # get number of state in processTestCase
    print STDERR "StateProcessTest $1\n";
    die "states $1 and $state dont match" unless $1 == $state;  # make sure it matches
    
    unless ($input =~ /$name: ([^\n]+)/g) {
        warn "testcase for state:$state doesnt exist";
        last;
    }
    $ret{$state}{$TEST} = $1;
    print STDERR "State: $state  status: test: $1\n";
}
for (sort keys %ret) {
    next if not defined $ret{$_}{$TEST};
#    die if not defined $ret{$_}{$FIRST};
#    print "$_ $ret{$_}{$FIRST}";

    if (defined $ret{$_}{$SECOND}) {
#        print " $ret{$_}{$SECOND}";
    } else {
#        print " -1";
    }

    if (defined $ret{$_}{$STATUS}) {
#        print " $ret{$_}{$STATUS} ";
    } else {
#        print " -1 ";
    }

    print $ret{$_}{$TEST};

    print "\n";
}

