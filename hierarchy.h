#ifndef _HIERARCHY_H_
#define _HIERARCHY_H_

#include <vector>
#include <string>

std::vector<std::string> get_hierarchy(std::string file_system);

#endif
