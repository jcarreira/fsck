#!/usr/bin/perl

# Given a file created by generate_tests.pl
# takes a disk and replaces the bytes to create a test disk
#


use strict;
use warnings;

if (scalar @ARGV != 5) {
    print "set_disk tests state sb address device";
    die "Wrong arguments";
}

my $msg  = $ARGV[0];
my $nstate = $ARGV[1];
my $sb   = $ARGV[2];
my $address   = $ARGV[3];
my $device = $ARGV[4];
my $name;
my $disk_out;
my $ntest = 0;

@ARGV = ("$msg");

&read_disk(); # read disk to $disk_out

while (<>) {
#    print "Processing line $.\n";
    /(\d+) (-?\d+) (-?\d+) (\d+) (.*)/g;
    my $state = $1;
    my $ret1 = $2;
    my $ret2 = $3;
    my $ret3 = $4;
    my $test = $5;

    if ($state != $nstate) { next; }

    my @values = &parse_test($test);
    &run_test($state, @values);
}

sub run_test {
    my $state = shift;
    my @values = @_;
    $ntest++;

#    print "Values: ", scalar(@values), "\n";
#die if scalar(@values) != 1024;

#    print "Address: $address \n";
    my $index = $address;

#    substr($disk_out, $index, 1) = chr(255);

    for (@values) {
#        print "val: ", chr(hex($_)), "\n";
        my $value = chr(hex($_));
#print $index;
        substr($disk_out, $index, 1) = $value;
#print "Changed index $index";
        $index++;
    }

#my $new_disk = "sb_out$ntest";
    my $new_disk = "sb_out$state";

    open FILE, ">$new_disk" or die "Couldnt open file: $!";
    binmode FILE;
    print FILE $disk_out;
    close FILE;

    system("dd status=noxfer if=$new_disk of=$device 2>/dev/null");
#system("$fsck -fpt /dev/ram2");
#    my $ret_val1 = $? & 128 ? -1 : $? >> 8;
#    print ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n";
#    system("$fsck -fpt /dev/ram2");
#    my $ret_val2 = $? >> 8;
#
#    printf "$ntest. state: %d ret1: %d  ret2: %d\n", $state, $ret_val1, $ret_val2;
}



sub parse_test {
    my $test = shift;

    die "not defined test" unless defined $test;

    my @ret;
    my @v = split /\s+/, $test;
    open FILE, ">>t_out" or die "Couldn't open file: $!";

    for (@v) {
        /.*(..)/g;
        push @ret, $1;
print FILE " $1 ";
    }
    
    close FILE;
    return @ret;
}

sub read_disk {
    local $/=undef;
    open FILE, $sb or die "Couldn't open file: $!";
    binmode FILE;
    $disk_out = <FILE>;
    close FILE;
}

