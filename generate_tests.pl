#!/usr/bin/perl

# Generate a file that holds:
# input - input that leads the app through a path
# return values - values returned by the fsck in several runs

use strict;
use warnings;

if (scalar @ARGV != 1 && @ARGV != 2) {
    print "generate_tests messages.txt <symbolic_name>";
    die "Wrong arguments";
}

my $msg  = $ARGV[0];
my $name = $ARGV[1];
my $FIRST = 1;
my $SECOND = 2;
my $STATUS = 3;
my $TEST = 4;
my %ret;
my $input;

@ARGV = ("$msg");

{
    local $/=undef;
    open FILE, $msg or die "Couldn't open file: $!";
    binmode FILE;
    $input = <FILE>;
    close FILE;
}

#while (1) {
    print STDERR "name: $name\n" if defined $name;
    if (not defined $name) {
        if($input =~ m/with name \'(.*)\'/) {
            $name = $1;
        }
    }
#}

pos($input) = 0;
print STDERR "Checking 1st\n";
while ($input =~ m/\[State (\d+)\].*fsck return value: (\d+)/g) {
    $ret{$1}{$FIRST} = $2;
    print STDERR "State: $1  ret: $2 \n";
}

pos($input) = 0;
print STDERR "Checking 2nd\n";
while ($input =~ m/\[State (\d+)\].*fsck2 return value: (\d+)/g) {
#    die "SECOND but not FIRST state $1" if not defined $ret{$1}{$FIRST};
    $ret{$1}{$FIRST} = -2 if not defined $ret{$1}{$FIRST};
    $ret{$1}{$SECOND} = $2;
    print STDERR "State: $1  ret2: $2 \n";
}

pos($input) = 0;
print STDERR "Checking 3rd\n";
while ($input =~ m/Terminating state (\d+) with (error )?message 'State was terminated by opcode/g) {
    print STDERR "State $1\n";
    my $state = $1;
    die "error matching state $state" unless ($input =~ m/status: (\d+)/g);
    my $status = $ret{$state}{$STATUS} = $1;
    die "error getting state: $state testcase" unless ($input =~ /processTestCase of state (\d+)/g);
    die "states $1 and $state dont match" unless $1 == $state;
    
    unless ($input =~ /$name: (.*)/g) {
        warn "testcase for state:$state doesnt exist";
        last;
    }
    $ret{$state}{$TEST} = $1;
    print STDERR "State: $state  status: $status \n";
}

for (sort keys %ret) {
    next if not defined $ret{$_}{$TEST};
#    die if not defined $ret{$_}{$FIRST};
    print "$_ $ret{$_}{$FIRST}";

    if (defined $ret{$_}{$SECOND}) {
        print " $ret{$_}{$SECOND}";
    } else {
        print " -1";
    }

    if (defined $ret{$_}{$STATUS}) {
        print " $ret{$_}{$STATUS} ";
    } else {
        print " -1 ";
    }

    print $ret{$_}{$TEST};

    print "\n";
}

