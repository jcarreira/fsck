#include <cstdio>
#include <cassert>
#include "hierarchy.h"
#include <string>
#include <vector>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <algorithm>
#include <stdarg.h>
#include <signal.h>
#include <sys/mount.h>

using namespace std;

//#define DEBUG

FILE* msg_fout;
char cwd[1000];

struct description {
    char description[1000];
    int pos1; // ext2
    int length1; //ext2

    int pos2; // minix
    int length2; // minix 
};

bool readDescription(FILE* fin, description& desc) {
    if (fscanf(fin, "%[^\n]", desc.description) == EOF) return 0;
    if (fscanf(fin, "%d", &desc.pos1) == EOF) return 0;
    if (fscanf(fin, "%d", &desc.length1) == EOF) return 0;
    if (fscanf(fin, "%d", &desc.pos2) == EOF) return 0;
    if (fscanf(fin, "%d", &desc.length2) == EOF) return 0;

    char line[10];
    fgets(line, 10, fin); // skip blank line

    return true;
}

void setPosFile(FILE *fout, char* description, int pos, int length) {
    fseek(fout, 0, SEEK_SET);
    fprintf(fout, "%s %d %d", description, pos, length); // description cant have spaces
    fflush(fout);
}

void message(const char* fmt, ...) {
    va_list ap;
    va_start(ap, fmt);

    char str[1000];

    vprintf(fmt, ap);
    va_start(ap, fmt);
    vsprintf(str, fmt, ap);
    //puts(str);
    fputs(str, msg_fout);
//    vfprintf(msg_fout, fmt, ap);
    fflush(msg_fout);
    fflush(stdout);

    va_end(ap);
}

//void message(char *str) {
//    puts(str);
//    fputs(str, msg);
//}

void check(const string& s, int a, int b, const char* desc) {
    if (a != b) {
        message("ERROR. Fields do not match at %s. desc: %s. %d != %d\n", s.c_str(), desc, a, b);
    }
}

void alarm_handler(int code) {
    message("ALARM!");
    system("pgrep qemu | xargs kill -9");
}

int main() {
    msg_fout = fopen("cmp_log", "w");
    FILE *fin = fopen("desc", "r");
    FILE *pos_ext2 = fopen("build_ext2/tests/pos.dat", "w");
    FILE *pos_minix = fopen("build_minix/tests/pos.dat", "w");
    assert(fin && pos_ext2 && pos_minix);

    system("umount /dev/ram1");
    system("umount /dev/ram2");

    signal(SIGALRM, alarm_handler);
    description desc;

    char* cwd = get_current_dir_name();
    assert(cwd);
    while (readDescription(fin, desc)) {
        message("Test: %s Pos1: %u Length1: %d Pos2: %d Length2: %d\n", desc.description, desc.pos1, desc.length1, desc.pos2, desc.length2);
        setPosFile(pos_ext2, desc.description, desc.pos1, desc.length1);
        message("Launching ext2 s2e..\n");

        assert(chdir("build_ext2/") == 0);
        system("./launchvm.sh ready -nographic -s2e-max-processes 8&");
        sleep(30);
        system("pgrep qemu | xargs kill -9");
        assert(chdir(cwd) == 0);

        message("s2e done\n");

        // Handle Minix NOW
        setPosFile(pos_minix, desc.description, desc.pos2, desc.length2);
        assert(chdir("build_minix/") == 0);
        message("Launching minix s2e..\n");
        //alarm(60);
        system("./launchvm.sh ready -nographic -s2e-max-processes 8&");
        sleep(30);
        system("pgrep qemu | xargs kill -9");
        assert(chdir(cwd) == 0);

        sleep(10);
        system("find build_ext2/s2e-last/* -name messages.txt | xargs cat >> messages_ext2.txt");
        system("find build_minix/s2e-last/* -name messages.txt | xargs cat >> messages_minix.txt");
        //        system("cp build_minix/s2e-last/messages.txt messages_minix.txt");

        sleep(5);
        system("./generate_tests.pl messages_ext2.txt > tests_ext2");
        system("./generate_tests.pl messages_minix.txt > tests_minix");

        FILE* test1 = fopen("tests_ext2", "r");
        FILE* test2 = fopen("tests_minix", "r");
        assert(test1 && test2);

        int state_ext2, ret1, ret2, ret3;
        char test[10000];

        system("mkdir fs_dir");
        system("mkdir build_minix/fs_dir");
        while (fscanf(test1, "%d %d %d %d %[^\n]", &state_ext2, &ret1, &ret2, &ret3, test) != EOF) {
            if (state_ext2 < 0) break;
            char command[1000];
            sprintf(command, "./set_disk.pl tests_ext2 %d sb_ext2 %d /dev/ram1", state_ext2, desc.pos1);
            puts(command);
            system(command); // put disk in test 'state' in /dev/ram1
            message("umount /dev/ram1\n");
            umount("/dev/ram1");
            umount("/dev/ram1");
            //system("umount /dev/ram1");
            //system("umount /dev/ram1");
            system("mount -t ext2 /dev/ram1 fs_dir");
            vector<string> paths_ext2 = get_hierarchy("fs_dir");
            message("umount /dev/ram1\n");
            umount("/dev/ram1");
            umount("/dev/ram1");
            //system("umount /dev/ram1");
            //system("umount /dev/ram1");

            int state_minix, ret1, ret2, ret3;
            while (fscanf(test1, "%d %d %d %d %[^\n]", &state_minix, &ret1, &ret2, &ret3, test) == 5) {
                char command[1000];
                sprintf(command, "./set_disk.pl tests_minix %d sb_minix %d /dev/ram2", state_minix, desc.pos2);
		puts(command);
                system(command); // put disk in test 'state' in /dev/ram1
                
                message("umount /dev/ram2\n");
                umount("/dev/ram2");
                //system("umount /dev/ram2");
                //system("umount /dev/ram2");
                assert(chdir("build_minix/") == 0);
                system("mount -t minix /dev/ram2 fs_dir");
                system("mount -t minix /dev/ram2 fs_dir");
                system("mount -t minix /dev/ram2 fs_dir");
                vector<string> paths_minix = get_hierarchy("fs_dir");
                message("cwd: %s\n", cwd);
                assert(chdir(cwd) == 0);
                message("umount /dev/ram2\n");
                umount("/dev/ram2");
                //system("umount /dev/ram2");
                //system("umount /dev/ram2");

                message("Testing state %d (ext2) against state %d (minix)\n", state_ext2, state_minix);

                sort(paths_ext2.begin(), paths_ext2.end());
                sort(paths_minix.begin(), paths_minix.end());

#ifdef DEBUG
                message("ext2 structure. size: %d\n", paths_ext2.size());
                for (int i = 0; i < (int)paths_ext2.size(); ++i) {
                    puts(paths_ext2[i].c_str());
                }
                message("minix structure. size: %d\n", paths_minix.size());
                for (int i = 0; i < (int)paths_minix.size(); ++i) {
                    puts(paths_minix[i].c_str());
                }
#endif

                if (paths_ext2 != paths_minix) {
                    message("ERROR: structures are not the same. %d %d. size1: %d size2: %d\n", state_ext2, state_minix, paths_ext2.size(), paths_minix.size());
                }

                for (int i = 0; i < (int)paths_ext2.size(); ++i) {
                    assert(paths_ext2[i] == paths_minix[i]);

                    message("Checking path %s\n", paths_ext2[i].c_str());

                    struct stat buf1, buf2;
                    stat(paths_ext2[i].c_str(), &buf1);
                    stat(paths_minix[i].c_str(), &buf2);

                    check(paths_ext2[i], buf1.st_nlink, buf2.st_nlink, "number of hard links");
                    check(paths_ext2[i], buf1.st_gid, buf2.st_gid, "group id");
                    check(paths_ext2[i], buf1.st_uid, buf2.st_uid, "user id");
                    check(paths_ext2[i], buf1.st_rdev, buf2.st_rdev, "device id");
                    check(paths_ext2[i], buf1.st_dev, buf2.st_dev, "id of device");
                    check(paths_ext2[i], buf1.st_mode, buf2.st_mode, "stat mode");
                }
            }

        }
        return 0;
    }
    free(cwd);
    message("Descriptions are over");
}
