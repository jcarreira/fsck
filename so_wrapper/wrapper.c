#define _GNU_SOURCE
#include <dlfcn.h>
#define _FCNTL_H
#include <bits/fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include "s2e.h"
#include <stdarg.h>
#include <stdio.h>

extern int errorno;
#define BLOCKS 20000
#define BLOCK_SIZE 1024
#define MAX_SIZE 10000
//#define CONCRETE
//#define DEBUG

char* symb_data;
char isDiskOpened = 0;
int fs_size;
char transientError = 0;
char badSectorBit = 0;

int symbPos;
int symbLength;
char isFdSymb[MAX_SIZE];
//unsigned long long int offsets[MAX_SIZE];

//unsigned int offset_disk = 0;

FILE* (*_fopen)(const char *path, const char *mode);
int (*_open)(const char * pathname, int flags, mode_t);
int (*_open64)(const char * pathname, int flags, mode_t);
off_t (*_lseek)(int fd, off_t offset, int whence);
ssize_t (*_write)(int fd, const void *buf, size_t count);
int (*_close)(int fd);
off64_t (*_lseek64)(int fd, off64_t offset, int whence);
ssize_t (*_read)(int fd, void *buf, size_t count);
void* (*_malloc)(size_t size);
int (*_llseek)(unsigned int fd, unsigned long offset_high,            unsigned
        long offset_low, loff_t *result,            unsigned int whence);

void progress(const char* str) {
    //puts(str);fflush(stdout);
#ifndef CONCRETE
    //message(str);
#endif
}

void message(const char* str) {
#ifndef CONCRETE
    //s2e_message(str);
#endif
//    puts(str);
}

void message_int(int n) {
#ifndef CONCRETE
    //s2e_message_int(n);
#endif
//    printf("%d\n", n);fflush(stdout);
}

void assert_model(char b, const char* msg) {
    if (!b) {
#ifndef CONCRETE
        char* str = "ERROR model: ";
        progress(str);
        s2e_kill_state(-2, msg);
#endif
        progress(msg);
        assert(0);
    }
}

__attribute__((constructor)) void foo(void) {
//void foo(void) {
    static int built = 0;
    if (built == 1) return;
    built = 1;

    progress("Constructor1");
    _open = (int (*)(const char * pathname, int flags, mode_t)) dlsym(RTLD_NEXT, "open");
    _open64 = (int (*)(const char * pathname, int flags, mode_t)) dlsym(RTLD_NEXT, "open64");
    _write = (ssize_t (*)(int fd, const void *buf, size_t count)) dlsym(RTLD_NEXT, "write");
    _close = (int (*)(int)) dlsym(RTLD_NEXT, "close");
    _lseek = (off_t (*)(int fd, off_t offset, int whence)) dlsym(RTLD_NEXT, "lseek");
    _llseek = (int (*)(unsigned int fd, unsigned long offset_high,            unsigned
           long offset_low, loff_t *result,            unsigned int whence)) dlsym(RTLD_NEXT, "_llseek");
    _lseek64 = (off64_t (*)(int fd, off64_t offset, int whence)) dlsym(RTLD_NEXT, "lseek64");
    _read = (ssize_t (*)(int fd, void *buf, size_t count)) dlsym(RTLD_NEXT, "read");
    _malloc = (void* (*)(size_t size)) dlsym(RTLD_NEXT, "malloc");
    _fopen = (FILE* (*)(const char* path, const char* mode)) dlsym(RTLD_NEXT, "fopen");
    progress("Constructor2");

    assert(_read);
    assert(_open);
    assert(_lseek64);
    assert(_lseek);
    assert(_write);
    assert(_close);
    assert(_malloc);
    progress("Constructor3");

    int filed = _open("test_disk", O_RDONLY, 0);
    s2e_assert(filed != -1, "SB read");
    progress("Constructor4");

    symb_data = (char*)_malloc(BLOCKS * BLOCK_SIZE); 
    assert_model(symb_data != 0, "symb_data != 0");
    progress("Constructor5");
    int rd = _read(filed, symb_data, BLOCKS * BLOCK_SIZE);
    assert_model(BLOCKS*BLOCK_SIZE >=rd, "space");
    assert_model(rd != -1, "rd != -1");
    progress("Read test_disk. bytes:");

#ifndef CONCRETE
    //message_int(rd);
#endif
    int size = _lseek(filed, 0, SEEK_END);
    printf("test_disk bytes: %d %d\n", rd, size);
    progress("Constructor6");
    fs_size = rd;
    _close(filed);
    progress("Constructor7");
}

void *malloc(size_t size) {
    return _malloc(size);
}

int close(int fd) {
    isFdSymb[fd] = 0;
    return _close(fd);
}

int open(const char * pathname, int flags, mode_t mode)
{
    message("OPEN.filename:");
    message(pathname);
    int fd = _open(pathname, flags, mode);

    printf("Opened file %s with fd=%d\n", pathname, fd);


    if (strcmp(pathname, "test_disk") != 0)
        return fd;

    printf("Opened disk with fd: %d\n", fd);

    isFdSymb[fd] = 1;
    if (isDiskOpened == 1)
        return fd;

    isDiskOpened = 1;

#ifndef CONCRETE
    //if (s2e_version() != 0) {
    if (1) {
        if (1) {
            progress("Getting sb from pos.dat.");
            FILE* fin = _fopen("pos.dat", "r");
            char description[100];
            int pos, length;
            assert(fin);

            while (fscanf(fin, "%s %d %d", description, &pos, &length) == 3) {
                progress(description);
                int i;
                //    printf("pos: %d\n", pos);
                //for (i = 0; i < 30;i++) {
                //    printf("i: %d\n", pos+i);
                //    *(symb_data+pos+i) = 0;
                //}
                //*(symb_data+pos+0) = 0;
                //*(symb_data+pos+1) = 0;
                //*(symb_data+pos+2) = 0;
                //*(symb_data+pos+3) = 0;
                //mempcy(symb_data+pos, data, 4);
                symbPos = pos;
                symbLength = length;
                message_int(pos);
                message_int(length);

                //char data[4] = {3,0,0,0};
                //s2e_make_symbolic(symb_data+pos+1, 1, description);
                //*(symb_data+pos) = 3;
                //*(symb_data+pos+1) = 0;
                //*(symb_data+pos+2) = 0;
                //*(symb_data+pos+3) = 0;
                //mempcy(symb_data+pos, data, 4);
                s2e_make_symbolic(symb_data+pos, length, description);
            }
            fclose(fin);
        }
    }
#endif
    return fd;
}

int open64_2(const char * pathname, int flags, mode_t mode) {
    assert(0);
}
int __open64_2(const char * pathname, int flags, mode_t mode) {
    return open64(pathname, flags, mode);
}
int open64(const char * pathname, int flags, mode_t mode)
{
    printf("OPEN64\n");
    return open(pathname, flags | O_LARGEFILE, mode);
    //assert(0);
    //progress("MYOPEN2");
    //int (*_open64)(const char * pathname, int flags, ...);
    //_open64 = (int (*)(const char * pathname, int flags, ...)) dlsym(RTLD_NEXT, "open64");
    //return _open64(pathname, flags, mode);
}



//off_t lseek32(int fd, off_t offset, int whence) {
//    assert_model(0, "lseek not supported");
//    progress("LSEEK. pos:");
//    message_int(offset);
//
//#ifdef BAD_SECTORS
//    if (transientError == 0) {
//        s2e_make_symbolic(badSectorBit, 1, "badSector");
//        if (badSectorBit == 1) {
//            transientError = 1; // we just want to fail once per run
//            errno = EIO;
//            return -1;
//        }
//    }
//#endif
//    
//    off_t ret = _lseek(fd, offset, whence);
//    if (ret != -1) {
//        if (whence == SEEK_SET) { offsets[fd] = offset; message("SEEK_SET");}
//        if (whence == SEEK_CUR) { offsets[fd] += offset; message("SEEK_CUR"); }
//        if (whence == SEEK_END) { assert_model(0, "SEEK_END not supported"); }
//    }
//    printf("disk fd: %d  seek: %lu ret: %lu\n", fd, offsets[fd], ret);
//
//    offsets[fd] = ret;
//    //int seek = _lseek(fd, SEEK_CUR, 0);
//
//    //message_int(seek);
//    return ret;
//}
//
//
//off_t lseek(int fd, off_t offset, int whence) {
//    assert_model(0, "lseek not supported");
//    progress("LSEEK. pos:");
//    message_int(offset);
//
//#ifdef BAD_SECTORS
//    if (transientError == 0) {
//        s2e_make_symbolic(badSectorBit, 1, "badSector");
//        if (badSectorBit == 1) {
//            transientError = 1; // we just want to fail once per run
//            errno = EIO;
//            return -1;
//        }
//    }
//#endif
//    
//    off_t ret = _lseek(fd, offset, whence);
//    if (ret != -1) {
//        if (whence == SEEK_SET) { offsets[fd] = offset; message("SEEK_SET");}
//        if (whence == SEEK_CUR) { offsets[fd] += offset; message("SEEK_CUR"); }
//        if (whence == SEEK_END) { assert_model(0, "SEEK_END not supported"); }
//    }
//    printf("disk fd: %d  seek: %lu ret: %lu\n", fd, offsets[fd], ret);
//
//    offsets[fd] = ret;
//    //int seek = _lseek(fd, SEEK_CUR, 0);
//
//    //message_int(seek);
//    return ret;
//}
//
//off64_t lseek64(int fd, off64_t offset, int whence) {
//    //if (fd < 0) return lseek32(-fd, offset, whence);
//    progress("\nLSEEK64. pos:");
//    message_int(offset);
//    printf("%d %lu %llu\n", offset, offset, offset);
//    if (!isFdSymb[fd])
//        return  _lseek64(fd, offset, whence);
//
//
//#ifdef BAD_SECTORS
//    if (transientError == 0) {
//        s2e_make_symbolic(badSectorBit, 1, "badSector");
//        if (badSectorBit == 1) {
//            transientError = 1; // we just want to fail once per run
//            errno = EIO;
//            return -1;
//        }
//    }
//#endif
//    
//
//    off64_t ret = _lseek64(fd, offset, whence);
//    return ret;
//
//    //printf("ret: %llu\n", ret);
//
//    //if (ret != -1) {
//    //    if (whence == SEEK_SET) { offsets[fd] = offset; message("SEEK_SET");}
//    //    if (whence == SEEK_CUR) { offsets[fd] += offset; message("SEEK_CUR"); }
//    //    if (whence == SEEK_END) { assert_model(0, "SEEK_END not supported"); }
//    //}
//    //printf("disk fd: %d  seek: %lu ret: %llu\n", fd, offsets[fd], ret);
//    //offsets[fd] = ret;
//    //offset_disk=ret;
//    //printf("disk fd: %d  seek: %d ret: %llu\n", fd, offsets[fd], ret);
//    //printf("disk fd: %d  seek: %d ret:  %llu\n", fd, offsets[fd], ret);
//    //printf("ret: %llu\n", ret);
//}

char B[100000];
ssize_t read(int fd, void *buf, size_t count) {
    //return _read(fd, buf, count);
    //assert(0);
    progress("READ1");

#ifdef BAD_SECTORS
    if (transientError == 0) {
        s2e_make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            transientError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif

    if (isFdSymb[fd] == 1) { // if this is a fd to our disk
        //message("Reading disk");
        //off64_t seek2 = _lseek64(fd, SEEK_CUR, 0);
        off64_t seek = _lseek64(fd, 0, SEEK_CUR);
        off64_t size = _lseek64(fd, 0, SEEK_END);
        _lseek64(fd, seek, SEEK_SET);
        //return _read(fd, buf, count);
        //off64_t seek = offsets[fd];
        //printf("seeked to: %llu\n", seek2);

        //message_int(seek2);
        //assert_model(seek2 == seek, "seek2 == seek");

        //int ret = count;
        //if (offsets[fd] + count >= fs_size) 
        //    ret = fs_size - offsets[fd];

        printf("fd: %d. Reading %d bytes at position %llu size of file: %llu\n", fd, count, seek, size);

#ifndef CONCRETE
        //s2e_message_int(fd);
        //s2e_message_int(count);
        //s2e_message_int(seek);
        //s2e_message_int(size);
#endif
        
        int readRet = _read(fd, B, count);
        //return readRet;
#ifdef DEBUG
        if (seek == 66048) {
            int i;
            for (i = 0; i < 512;i++)
                printf("%d ", B[i]);
            puts("");
        }
#endif
        memcpy(buf, symb_data + seek, readRet);
#ifdef DEBUG
        if (seek == 66048) {
            int i;
            for (i = 0; i < 512;i++)
                printf("%d ", symb_data[seek+i]);
            puts("");
        }
#endif
//#ifdef CONCRETE
//        assert_model(memcmp(B, symb_data+seek, readRet) == 0, "read is equal to file");
//#else
//        if (!(symbPos >= seek && symbPos <= seek+count) &&memcmp(B, symb_data+seek, readRet) != 0) {
//           message_int(fd);
//           message_int(seek);
//           message_int(count);
//           assert_model(0, "0");
//        }
//#endif
//
        //if (seek <= 65536 && symb_data+seek+count >=65540)
        //    printf("READING special bytes. %c %c %c %c\n", symb_data[65536], symb_data[65537], symb_data[65538], symb_data[65539]);

        //offsets[fd]+=count;
        
        message_int(readRet);
        //message_int(ret);
        //assert_model(readRet == ret, "readRet == ret");
        return readRet;
    } else {
        int ret = _read(fd, buf, count);
        printf("Read %d bytes. count=%d fd=%d", ret, count, fd);
        return ret;
    }
}

ssize_t write(int fd, const void *buf, size_t count) {
    //assert(0);
    progress("WRITE");

#ifdef BAD_SECTORS
    if (transientError == 0) {
        s2e_make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            transientError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif

    off64_t seek = _lseek64(fd, 0, SEEK_CUR);


    //int written = _write(fd, buf, count);
    int written = count;
    //printf("Wrote %d bytes to fd: %d offset: %llu\n", count, fd, seek);
    if (isFdSymb[fd] == 1) { // if this is a fd to our disk
        //off64_t seek = _lseek64(fd, 0, SEEK_CUR);
        printf("Writing %d bytes at pos: %d\n", written, seek);
        printf("FD: %d\n", fd);
        //int seek = offsets[fd];
        memcpy(symb_data + seek, buf, written);
#ifdef DEBUG
        if (seek == 66048) {
            int i;
            for (i = 0; i < 512;i++)
                printf("%d ", symb_data[seek+i]);
            puts("");
        }
#endif
        //offsets[fd] += count;
        //return count;
    } 
    //assert_model(written == count, "written == count");
    return written;
}

// ssize_t write_m(int fd, const void *buf, size_t count) {
//     //progress("WRITE");
//     progress_int(count);
//     progress_int(files[fd].seek);
//     //assert_model(fd >= 0 && fd < FSIZE, "write: fd not in the range");
//     //progress("WRITE1");
//     //assert_model(files[fd].content != 0, "write:file not open");
//     //progress("WRITE2");
//     //assert_model(files[fd].seek + count < files[fd].size, "write: not enough file space");
//     
// #ifdef BAD_SECTORS
//     if (transientError == 0) {
//         s2e_make_symbolic(badSectorBit, 1, "badSector");
//         if (badSectorBit == 1) {
//             transientError = 1; // we just want to fail once per run
//             errno = EIO;
//             return -1;
//         }
//     }
// #endif
// 
//     //progress("WRITE3");
//     //assert_model(fd == _FD, "fd == _FD");
//     memcpy(files[fd].content + files[fd].seek, (char*)buf, count);
//     //progress("WRITE4");
//     files[fd].seek += count;
//     return count;
// }
FILE *freopen(const char *path, const char *mode, FILE *stream) {
    assert(0);
}
FILE* fopen(const char *path, const char *mode) {
    assert(0);
    progress("FOPEN:");
    progress(path);
    return _fopen(path, mode);
}

int printf(const char *format, ...) {
    return 1;
}

