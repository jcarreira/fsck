#include <stdio.h>
#include <string.h>
#include "s2e.h"

int main(void)
{
    int num;
    // printf("Enter two characters: ");
    // if(!fgets(str, sizeof(str), stdin))
    //   return 1;

    printf("s2e_version: %d\n", s2e_version());fflush(stdout);
    s2e_disable_all_apic_interrupts();
    s2e_enable_forking();
    s2e_make_symbolic(&num, sizeof(int), "num");

    printf("here\n");
    int a = 5 / num;

    s2e_disable_forking();

    s2e_get_example(&num, sizeof(int));
    printf("'%d'\n", num);

    s2e_enable_all_apic_interrupts();
    s2e_kill_state(0, "program terminated");

    return 0;
}

