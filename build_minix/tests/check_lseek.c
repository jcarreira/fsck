#include <cstdio>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include "s2e.h"

const char* mknod_str = "mknod /dev/ram5 -m 660 b 1 1";
const char* dd_str    = "dd if=/dev/zero of=/dev/ram5 bs=4096 count=40";
//const char* fsck_str  = "./e2fsck -n -B 4096 /dev/ram5";
const char* fsck_str  = "./fsck.minix -f /dev/ram5";
const char* mount_str = "mount /dev/ram5 /mnt/ext2";
const char* dmesg_str = "dmesg";
const char* free_str  = "free -m";
const char* nblocks_str = "dd bs=4 seek=257 count=1 if=hex of=/dev/ram5";
    int a;

int calc(int* b) {
    if (*b>0) {
        s2e_print_expression("qwe", *b);
        return 1;
    } else {
        s2e_print_expression("qwe", *b);
        return 2;
    }
}

int main() {
    //s2e_disable_all_apic_interrupts();
    s2e_enable_forking();

    s2e_make_symbolic(&a, sizeof(int), "a");

    if (a>0) {
        s2e_get_example(&a, sizeof(int));
        s2e_print_expression("if1", a);

if (a<-10) {

        s2e_get_example(&a, sizeof(int));
s2e_print_expression("if3", a);
}

    } else {

        s2e_get_example(&a, sizeof(int));
    s2e_print_expression("if2", a);
    }

    //calc(&a);

    s2e_print_expression("qwe", a);

    //puts(mknod_str);
    //system(mknod_str);

    //puts("lseeking");
    //int f = open("/dev/ram5", O_RDWR);
    //if (f) s2e_message("SUCCESS2");
    //lseek(f, 1024*5, SEEK_SET);
    
    s2e_disable_forking();
    //s2e_enable_all_apic_interrupts();
    s2e_kill_state(0, "TERMINATED");

    return 0;
}

