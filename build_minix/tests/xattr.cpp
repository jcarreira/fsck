#include <cstdio>
#include <cstring>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "s2e.h"
#include <string>
#include <vector>
#include <stdlib.h>
#include <errno.h>
#include <sys/vfs.h>
#include <dirent.h>
#include <attr/xattr.h>

using namespace std;

#define SIZE 70
#define SIZE_DIR 300
#define SIZE_SYM1 1
#define SIZE_SYM2 1

int errno1, errno2;
int ret1, ret2;
char value[SIZE] = {0};
char attr[SIZE] = {0};
static const char* DIR_1 = "/mnt/ext4/asd";
static const char* DIR_2 = "/mnt/ntfs/asd";

void print_sym_vars() {
    s2e_get_example(&ret1, sizeof(int));
    s2e_get_example(&ret2, sizeof(int));
    s2e_get_example(&errno1, sizeof(int));
    s2e_get_example(&errno2, sizeof(int));
    char msg[100];
    sprintf(msg, "%d == %d %d == %d", ret1, ret2, errno1, errno2);
    s2e_message(msg);
}

void end() {
    print_sym_vars();
    s2e_disable_forking();
    s2e_enable_all_apic_interrupts();
    s2e_kill_state(0, "TERMINATED");
    exit(0);
}

void assert(int n, const char* msg) {
    if (n != 0) return;

    s2e_message("Error!!!");
    s2e_message(msg);
    print_sym_vars();
    s2e_kill_state(0, "error in assert");
}


int main(void)
{
    printf("s2e_version: %d\n", s2e_version());
    fflush(stdout);

    s2e_disable_all_apic_interrupts();
    s2e_enable_forking();

    attr[SIZE-1] = value[SIZE-1] = 0;
    //s2e_make_symbolic(value, SIZE - 1, "value");
    //s2e_make_symbolic(attr, SIZE - 1, "attr");
    strcpy(value, "a");
    strcpy(attr, "a");

    s2e_message("set_attr");
    int len = strlen(value);
    char len_str[100];
    //sprintf(len_str, "len: %d", len);
    //s2e_message(len_str);
    ret1 = setxattr(DIR_1, attr, value, len, 0); 
    errno1 = errno;
    s2e_message("set_attr2");
    //ret2 = setxattr(DIR_2, attr, value, len, 0); 
    errno2 = errno;

    assert(ret1 == ret2, "Retornos iguais");

    if (ret1 == -1) 
        assert(errno1 == errno2, "Errno iguais");

    // print value of return
    char retval[100];
    //s2e_get_example(&permissions, sizeof(mode_t));
    //s2e_get_example(dir_name1+strlen(DIR_), 10);
    sprintf(retval, "ret1: %d ret2: %d", ret1, ret2);
    s2e_message(retval);

    end();
    return 0;
}

