#include <cstdio>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include "s2e.h"

const char* mknod_str = "mknod /dev/ram5 -m 660 b 1 1";
const char* dd_str    = "dd if=/dev/zero of=/dev/ram5 bs=4096 count=40";
//const char* fsck_str  = "./e2fsck -n -B 4096 /dev/ram5";
const char* fsck_str  = "./fsck.minix -f /dev/ram5";
const char* mount_str = "mount /dev/ram5 /mnt/ext2";
const char* dmesg_str = "dmesg";
const char* free_str  = "free -m";
const char* nblocks_str = "dd bs=4 seek=257 count=1 if=hex of=/dev/ram5";

int main() {
    //s2e_disable_all_apic_interrupts();
    s2e_enable_forking();

    puts("Syncing");
    sync();
    
    s2e_disable_forking();
    //s2e_enable_all_apic_interrupts();
    s2e_kill_state(0, "TERMINATED");

    return 0;
}

