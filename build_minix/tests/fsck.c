#include <cstdio>
#include <stdlib.h>
#include <sys/wait.h>
#include "s2e.h"
#include <unistd.h>
 
const char* mknod_str = "mknod /dev/ram5 -m 660 b 1 1";
const char* dd_str    = "dd if=/dev/zero of=/dev/ram5 bs=4096 count=10";
const char* fsck_str  = "nice -n -19 ./fsck.minix -fa -Z /dev/ram5";
const char* gdb_str  = "nice -n -19 gdb ./fsck.minix < cmds";
const char* cat_str = "echo \"r -fa -Z /dev/ram5\" > cmds";
const char* mount_str = "mount /dev/ram5 /mnt/ext2";
const char* dmesg_str = "dmesg";
const char* free_str  = "free -m";
const char* nblocks_str = "dd bs=4 seek=257 count=1 if=hex of=/dev/ram5";
const char* getsb_str  = "./s2eget sb";
const char* getpos  = "./s2eget pos.dat";
const char* copysb_str = "dd if=sb of=/dev/ram5 bs=1 seek=1024";
const char* getfsck = "./s2eget Z";
const char* mvfsck  = "mv Z fsck.minix";

void progress(const char* s) { puts(s); s2e_message(s); }

int returns[100];

void create_fs() {
    system(mount_str);
    system("");
}

int main() {
    int ret_value = 0;

    progress("MINIX");

    // get file system checker
    progress(getfsck);
    system(getfsck);
    progress(mvfsck);
    system(mvfsck);

    // get pos.dat
    progress(getpos);
    system(getpos);

    // get superblock
    progress(getsb_str);
    system(getsb_str);

    // create dev
    progress(mknod_str);
    system(mknod_str);

    s2e_enable_forking();

    // init checks:
    // (get values on disk)
    //init_checks("minix_specification.txt", "/dev/ram5");

    // call fsck
    progress(fsck_str);
    int ret = system(fsck_str);
    //progress(cat_str);
    //system(cat_str);
    //progress(gdb_str);
    //int ret = system(gdb_str);

    char out[1000];
    ret = WEXITSTATUS(ret);
    sprintf(out, "fsck return value: %d\n", ret);
    s2e_message(out);

    returns[0] = ret;

//    for (int i =1;1;i++) {
//        change_phase();
//        progress("Running fsck again..");
//        progress(fsck_str);
//        int ret1 = system(fsck_str);
//        ret1 = WEXITSTATUS(ret1);
//        sprintf(out, "fsck%d return value: %d\n", i+1, ret1);
//        s2e_message(out);
//
////        if (ret1 == 0 || ret1 == 3) {
////            progress("Checking mountability");
////            check_mountability("diskToMount", progress);
////        }
//
//        check_properties("/dev/ram5");
//
//        returns[i] = ret1;
//        if (returns[i] == returns[i-1] || ret1 == 0)
//            break;
//    }

    //if (ret == 3 || ret == 7 || ret == 4) {
    //    change_phase();
    //    progress("Running fsck again..");
    //    progress(fsck_str);
    //    int ret1 = system(fsck_str);
    //    ret1 = WEXITSTATUS(ret1);
    //    sprintf(out, "fsck2 return value: %d\n", ret1);
    //    s2e_message(out);

    //    if (ret == 3 && ret1 != 0) {
    //        ret_value = 1;
    //        s2e_message("ERROR: ret = 3 && ret1 != 0");
    //    } else if (ret == 7 && ret1 != 4) {
    //        ret_value = 2;
    //        s2e_message("ERROR: ret = 7 && ret1 != 4");
    //    } else if (ret == 4 && ret1 != 4) {
    //        ret_value = 3;
    //        s2e_message("ERROR: ret = 4 && ret1 != 4");
    //    } else 
    //        ret_value = 10;
    //}
   
    s2e_disable_forking();
    s2e_kill_state(ret_value, "TERMINATED");

    return 0;
}

