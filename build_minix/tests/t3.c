#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/stat.h>
//#include <assert.h>
#include "s2e.h"

#define FILE "/mnt/ramdisk/file2"

void assert(int n, char* msg) {
    if (n == 1) return;

    s2e_message(msg);
    s2e_message("Error!!!");
    s2e_kill_state(0, "error in assert");
}

int main(void)
{
    char buf[100]={0};
    char toWrite[100]={0};

    printf("s2e_version: %d\n", s2e_version());fflush(stdout);
    s2e_disable_all_apic_interrupts();
    s2e_enable_forking();
    s2e_make_symbolic(&toWrite, 2, "to_write");
    toWrite[2] = 0;
    toWrite[0] = 'b';

    s2e_message("creat");
    int fd=creat(FILE, O_CREAT);
    assert(fd!=-1, "creat");
    s2e_message("write");
    assert(write(fd, toWrite, strlen(toWrite)) != -1, "write");
    s2e_message("close");
    close(fd);

    s2e_message("open");
    fd = open(FILE, O_RDONLY);
    assert(fd != -1, "open");
    s2e_message("read");
    read(fd, buf, strlen(toWrite));

    s2e_message("strcmp");
    assert (strcmp(buf, toWrite) == 0, "equal reads");
    s2e_message("2close");
    close(fd);



    s2e_disable_forking();
    s2e_enable_all_apic_interrupts();
    
    s2e_concretize(&toWrite, 2);
    s2e_message("content: ");
    s2e_message(toWrite);
    s2e_kill_state(0, "TERMINATED");

    return 0;
}

