-- File: config.lua
s2e = {
    kleeArgs = {
        -- Run each state for at least 1 second before
            -- switching to the other:
            --"--use-dfs-search=true",
	    --"--use-batching-search=true", "--batch-time=40.0",
            "--recovery-batch-time=60",
            --"--use-batching-search", "--use-non-uniform-random-search", "--batch-time=40.0",
            "--use-query-log",
            "--use-query-pc-log",  
            "--use-stp-query-pc-log", 
            "--fork-on-symbolic-address", 
            "--max-forks-on-concretize=10", 
            "--use-forked-stp", 
            "--max-stp-time=1",
            "--flush-tbs-on-state-switch=false",
            "--state-shared-memory=true"
    }
}

plugins = {
    "HostFiles",
    "BaseInstructions",
    "ExecutionTracer",
    "RawMonitor",
    "ModuleTracer",
    "TranslationBlockTracer",
     "ModuleExecutionDetector",
    "TestCaseGenerator",
    "RecoverySearcher",
}

pluginsConfig = {}

pluginsConfig.RawMonitor = {
    kernelStart = 0xc0000000,
    fsck = {
        name = "Z",
        start = 0x0,
        size = 1297806,
        nativebase = 0x8048000,
        delay = true,
        kernelmode = false   
    },
    libc = {
        name = "libc-2.11.2.so",
        start = "0x0",
        size = 1323460,
        nativebase = 0x16dd0,
        delay = true,
        kernelmode = false,
    },

}

pluginsConfig.ModuleExecutionDetector = {
    fsck = {
        moduleName = "Z",
        kernelMode = false,
    },
    libc = {
        moduleName = "libc-2.11.2.so",
        kernelMode = false,
    },
}

pluginsConfig.TranslationBlockTracer = {
  manualTrigger = false,
  flushTbCache = true
}

pluginsConfig.HostFiles = {
    baseDir = "tests/"
}
