#!/usr/bin/perl

use strict;
use warnings;

my $dir = shift @ARGV;

die if not defined $dir;

my $ret = `wc -l $dir/0/messages.txt`;

$ret =~ /(\d+)/;
my $lines = $1;

if ($lines != 15) {
    exit 1; # not stalled
} else {
    exit 0; # stalled
}
