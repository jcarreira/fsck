/*
 * S2E Selective Symbolic Execution Framework
 *
 * Copyright (c) 2010, Dependable Systems Laboratory, EPFL
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Dependable Systems Laboratory, EPFL nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE DEPENDABLE SYSTEMS LABORATORY, EPFL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Main authors: Vitaly Chipounov, Volodymyr Kuznetsov.
 * All S2E contributors are listed in the S2E-AUTHORS file.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "s2e.h"

#define check_zero(N) if (N == 0) { s2e_kill_state(-1, "division by zero"); }

#if 0
void s2e_get_example(void* buf, int size){}
unsigned int s2e_get_example_uint(unsigned int val) { return val; }
void s2e_disable_all_apic_interrupts(){}
void s2e_enable_all_apic_interrupts(){}
void s2e_message(const char* m) { puts(m);};
int s2e_version() { return 0;} 
void s2e_kill_state(int status, const char* message){exit(-1);}
void s2e_concretize(void* buf, int size){}
void s2e_make_symbolic(void* buf, int size, const char* name){}
void s2e_print_expression(const char* name, int expression){printf("%s %d:\n", name, expression);}
void s2e_message_int(int n) {printf("%d\n", n);}
void s2e_assert(int cond, const char* msg) {}
 void s2e_message_int2(const char* msg, int n) { char num[1000]; sprintf(num, "%s %d", msg, n); s2e_message(num); }
#endif

#if 1

 void change_phase(void)
{
    s2e_message("change phase");
    __asm__ __volatile__(
        ".byte 0x0f, 0x3f\n"
        ".byte 0x00, 0x61, 0x00, 0x00\n"
        ".byte 0x00, 0x00, 0x00, 0x00\n"
    );
}

 void increase_prio(void)
{
    __asm__ __volatile__(
        ".byte 0x0f, 0x3f\n"
        ".byte 0x00, 0x60, 0x00, 0x00\n"
        ".byte 0x00, 0x00, 0x00, 0x00\n"
    );
}

/** Get S2E version or 0 when running without S2E. */
  int s2e_version()
{
    int version;
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : "=a" (version)  : "a" (0)
            );
    return version;
}

/** Print message to the S2E log. */
void s2e_message(const char* message)
{
    //return;
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x10, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (message)
            );
}

 void s2e_message_int2(const char* msg, int n) {
    char num[1000];
    sprintf(num, "%s %d", msg, n);
    s2e_message(num);
}

  void s2e_message_int(int n) {
    char num[100];
    sprintf(num, "%d", n);
    s2e_message(num);
}

/** Print warning to the S2E log and S2E stdout. */
  void s2e_warning(const char* message)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x10, 0x01, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (message)
            );
}

/** Print symbolic expression to the S2E log. */
  void s2e_print_expression(const char* name, int expression)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x07, 0x01, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (expression), "c" (name)
            );
}

/** Enable forking on symbolic conditions. */
  void s2e_enable_forking(void)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x09, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            );
}

/** Disable forking on symbolic conditions. */
  void s2e_disable_forking(void)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x0a, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            );
}

/** Fill buffer with unconstrained symbolic values. */
  void s2e_make_symbolic(void* buf, int size, const char* name)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x03, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (buf), "b" (size), "c" (name)
            );
}

/** Concretize the expression. */
  void s2e_concretize(void* buf, int size)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x20, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (buf), "b" (size)
            );
}

/** Get example value for expression (without adding state constraints). */
  void s2e_get_example(void* buf, int size)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x21, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (buf), "b" (size)
            );
}

/** Get example value for expression (without adding state constraints). */
  unsigned s2e_get_example_uint(unsigned val)
{
    unsigned buf = val;
    __asm__ __volatile__(
            "pushl %%ebx\n"
            "movl %%edx, %%ebx\n"
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x21, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            "popl %%ebx\n"
            : : "a" (&buf), "d" (sizeof(val)) : "memory"
            );
    return buf;
}

/** Terminate current state. */
  void s2e_kill_state(int status, const char* message)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x06, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (status), "b" (message)
            );
}

  void s2e_load_module(const char* name,
        unsigned int loadbase, unsigned int size)
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0xAA, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : : "a" (name), "b" (loadbase), "c" (size)
            );
}

/** Disable timer interrupt in the guest. */
  void s2e_disable_timer_interrupt()
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x50, 0x01, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            );
}

/** Enable timer interrupt in the guest. */
  void s2e_enable_timer_interrupt()
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x50, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            );
}

/** Disable all APIC interrupts in the guest. */
  void s2e_disable_all_apic_interrupts()
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x51, 0x01, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            );
}

/** Enable all APIC interrupts in the guest. */
  void s2e_enable_all_apic_interrupts()
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x51, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            );
}

/** Declare a merge point: S2E will try to merge
 *  all states when they reach this point.
 *
 * NOTE: This requires merge searcher to be enabled. */
  void s2e_merge_point()
{
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0x70, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            );
}

/** Open file from the guest.
 *
 * NOTE: This require HostFiles plugin. */
  int s2e_open(const char* fname)
{
    int fd;
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0xEE, 0x00, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : "=a" (fd) : "a"(-1), "b" (fname), "c" (0)
            );
    return fd;
}

/** Close file from the guest.
 *
 * NOTE: This require HostFiles plugin. */
  int s2e_close(int fd)
{
    int res;
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0xEE, 0x01, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : "=a" (res) : "a" (-1), "b" (fd)
            );
    return res;
}

/** Read file content from the guest.
 *
 * NOTE: This require HostFiles plugin. */
  int s2e_read(int fd, char* buf, int count)
{
    int res;
    __asm__ __volatile__(
            ".byte 0x0f, 0x3f\n"
            ".byte 0x00, 0xEE, 0x02, 0x00\n"
            ".byte 0x00, 0x00, 0x00, 0x00\n"
            : "=a" (res) : "a" (-1), "b" (fd), "c" (buf), "d" (count)
            );
    return res;
}

void s2e_assert(int cond, const char* msg) {
    if (!cond) {
        s2e_message("s2e_assert");
        s2e_kill_state(-1, msg);
    }
}

/** Raw monitor plugin */ 
  void s2e_rawmon_loadmodule(const char *name, unsigned loadbase, unsigned size)
{
    __asm__ __volatile__(
        "pushl %%ebx\n"
        "movl %%edx, %%ebx\n"
        ".byte 0x0f, 0x3f\n"
        ".byte 0x00, 0xAA, 0x00, 0x00\n"
        ".byte 0x00, 0x00, 0x00, 0x00\n"
        "popl %%ebx\n"
        : : "a" (name), "d" (loadbase), "c" (size)
    );
}

#endif
