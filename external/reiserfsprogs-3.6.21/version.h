/*
 * Copyright 2002-2004 Hans Reiser, licensing governed by reiserfsprogs/README
 */

#define print_banner(prog) \
fprintf(stderr, "%s %s (2009 www.namesys.com)\n\n", prog, VERSION)
