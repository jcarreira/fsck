#include <stdio.h>
#include <string.h>
#include "models.h"
#include "minix.h"
#include "s2e.h"

#define die(a) exit(-1);

static const char * program_name = "fsck.minix";
static char * device_name = NULL;
static int IN;
static int force = 0, show, repair;

static char * inode_buffer = NULL;

static char *super_block_buffer;
#define Super (*(struct minix_super_block *)super_block_buffer)
#define IMAPS ((unsigned long)Super.s_imap_blocks)
#define ZMAPS ((unsigned long)Super.s_zmap_blocks)

static char *inode_map;
static char *zone_map;

static void
read_block(unsigned int nr, char * addr) {
    lseek(IN, BLOCK_SIZE*nr, SEEK_SET);
    read_m(IN, addr, BLOCK_SIZE);
}

static void
read_superblock(void) {
    lseek(IN, BLOCK_SIZE, SEEK_SET);
    super_block_buffer = (char*)calloc(1, BLOCK_SIZE);
    read_m(IN, super_block_buffer, BLOCK_SIZE);
}

static void
read_tables(void) {
    inode_map = (char*)malloc(100 * BLOCK_SIZE);
    zone_map = (char*)malloc(100 * BLOCK_SIZE);
    memset(inode_map,0,sizeof(inode_map));
    memset(zone_map,0,sizeof(zone_map));
    inode_buffer = (char*)malloc(10000);


    s2e_message("MINIX: stage4.1.3");
    read_m(IN,inode_map,IMAPS*BLOCK_SIZE);
    //read_m(IN,inode_map,BLOCK_SIZE);
    s2e_message("MINIX: stage4.1.4");
    //exit(0);
    s2e_print_expression("IMAP", IMAPS);
    s2e_print_expression("ZMAP", ZMAPS);

    read_m(IN,zone_map,ZMAPS*1);//BLOCK_SIZE);
}



int main(int argc, char ** argv) {
    const char* fake_file = "sb";
    program_name = "fsck.minix";
    device_name = argv[0];
    //if (argc > 2) fake_file = argv[2];
    force=1;
    IN = open_m(device_name,repair?O_RDWR:O_RDONLY, "sb");
    read_superblock();
    read_tables();
    return 0;
}
