#ifndef _MODELS_H_
#define _MODELS_H_

#include <stddef.h>
#include "s2e.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <cassert>

#define FSIZE 300

static int BLOCK_SIZE = 1024;

struct file {
    char name[40];
    unsigned int seek;
    char* content;
    unsigned int size;
    char bspecial;  // should this be made symbolic?
    int mode;
    bool* blocks_symbolic;
};

unsigned int off=0;

file files[FSIZE]; // one file for each initial char

// ignore mode
int open_m(const char *filename, int mode, const char* sb_file) {
    int fd = (int)filename[0];

    files[fd].seek = 0;
    files[fd].content = (char*)malloc(100 * BLOCK_SIZE); 
    files[fd].size = 100 * BLOCK_SIZE;

    files[fd].blocks_symbolic = (bool*)calloc(sizeof(bool)*100, 1);

    for (int i = 0; i < files[fd].size;i++)
        files[fd].content[i] = 0;

    FILE* fin = fopen(sb_file, "r");
    int rd = fread(files[fd].content, 1, 10*BLOCK_SIZE, fin); // superblock is the second block
    
    if (s2e_version() != 0) {
        s2e_make_symbolic(files[fd].content+1028, 2, "imapblocks");
    }

    return fd;
}

off_t lseek(int fd, off_t offset, int whence) {
    if (whence == SEEK_SET) {
        files[fd].seek = offset;
    } else if (whence == SEEK_CUR) {
        files[fd].seek += offset;
    } else if (SEEK_END) {
        s2e_kill_state(-1, "ERROR: SEEK_END not implemented");
    } else s2e_kill_state(-1, "ERROR: whence not found.");

    return files[fd].seek;
}

ssize_t read_m(int fd, void *buf, size_t count) {
    int ret = count;
    s2e_message("readm1");
    s2e_print_expression("count", count);
    s2e_print_expression("seek", files[fd].seek);
    s2e_message(fd);

    if (files[fd].seek + count > files[fd].size) {
        count = files[fd].size - files[fd].seek;
        ret = 0;
    }

    s2e_message("readm2");
    char *vec = (char*)buf;
    for (int i = files[fd].seek; i < files[fd].seek + count; i++) {
        s2e_message("readm7 moving byte");
        s2e_print_expression("content byte", files[fd].content[i]);
        *vec++ = files[fd].content[i];

    }
    s2e_message("readm3");
    files[fd].seek += count;
    s2e_message("readm4");
    s2e_print_expression("seek", files[fd].seek);
    s2e_message(fd);
    return ret;
}
#endif
