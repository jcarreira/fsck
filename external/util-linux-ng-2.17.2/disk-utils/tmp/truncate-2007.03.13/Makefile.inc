# Copyright (c) 2006  Peter Pentchev
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

# $Ringlet: Makefile.inc 757 2006-11-16 13:30:34Z roam $

PROG=		truncate

SRCS=		truncate.c

PREFIX?=	/usr/local
BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man
ETCDIR?=	${PREFIX}/etc
DBDIR?=		/var/db/${PROG}

CC?=		gcc
WARNS?=		2
CFLAGS_WARN=	-Wall -W -Wstrict-prototypes -Wmissing-prototypes \
		-Wwrite-strings #${BDECFLAGS}
CFLAGS_INC=	-I. -I..
# CFLAGS_OPT and CFLAGS_DBG must be defined in OS-dependent Makefiles
CFLAGS_MISC=	-D_GNU_SOURCE

CFLAGS=		${CFLAGS_WARN} ${CFLAGS_OPT} ${CFLAGS_DBG} ${CFLAGS_COMPAT}
CFLAGS+=	${CFLAGS_INC} ${CFLAGS_OS} ${CFLAGS_MISC}

LDADD=
DPADD=

RM?=		rm -f
MKDIR?=		mkdir -p
SETENV?=	env
COMPRESS_CMD?=	gzip -cn9
SED?=		sed

# GNU make hack
.CURDIR?=	.

all:		${PROG}
