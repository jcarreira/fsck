#ifndef _MODELS_H_
#define _MODELS_H_

#include <errno.h>
#include <stddef.h>
#include "s2e.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include "utils.h"

//#define KILL_UNREC_ERRORS

#define BSIZE 1024
#define FSIZE 300
#define MEM_SIZE (10*1024*1024)
#define MAX_SIZE (200 * BSIZE)
#define MAX_BADSECTOR_IO 10000
//#define BAD_SECTORS 1

int badSectorIO_index = 0;
char badSectorBit;
char latentError = 0; // have we failed so far?

static int symbFromFile = 0;
const int SYMB_N = 1;
const int SYMB_ADDRESS[] = {1024};
const int SYMB_SIZE[] = {9216};
const char* SYMB_DESC[] = {"symbolic minix"};

typedef struct region {
    int size;
    char data[MAX_SIZE];
}region;

struct file {
    char name[40];
    unsigned int seek;
    char* content;
    unsigned int size;
    int mode;
    char written; // tell if data was written
};

const char* SHMEM_NAME = "sb_shmem";
struct file files[FSIZE];
static int RUN = 0;

void make_symbolic(void* data, int length, const char* description) {
    s2e_make_symbolic(data, length, description);
}

void assert_model(char b, const char* msg) {
    if (!b) {
        if (s2e_version() != 0) {
            s2e_message("ERROR model: ");
            s2e_kill_state(-2, msg);
        } else {
            puts(msg);
            assert(0);
        }
    }
}

void init_model(int bs, char fromFile) {
    symbFromFile = fromFile;
    //    badSectorBits[0] = 0; // touch it
    //    for (int i = 0; i < 100; ++i) {
    //        char name[30];
    //        sprintf(name, "badSector%d", i);
    //    }
}

int open_m(const char *filename, int mode, const char* sb_file) {
    assert_model(filename[0] != 0, "filename invalid");
    assert_model(BSIZE != 0, "BS is zero");

    int fd = (int)filename[0];
    int rd;

    assert_model(files[fd].name[0] == 0, "file with initial char already opened.");

    strcpy(files[fd].name, filename);
    files[fd].seek = 0;
    files[fd].mode = mode;

    files[fd].content = (char*)malloc(100 * BSIZE); 


    for (int i = 0; i < files[fd].size;i++)
        files[fd].content[i] = 0;

    int shmfd = shm_open(SHMEM_NAME, O_RDONLY, S_IRUSR);
    if (shmfd == -1) { // first run. get superblock from file 'sb'
        s2e_message_int(errno);
        s2e_message("First run. Getting sb from file.");
        RUN = 1;
        int filed = open(sb_file, mode);
        s2e_assert(filed != -1, "SB read");
        rd = read(filed, files[fd].content, 100*BSIZE);
        //for (int i = 0; i < 3000; ++i) {
        //    s2e_message_int(i);
        //    //s2e_print_expression("file.content[i]", files[fd].content[i]);
        //}
        files[fd].size = rd;
        close(filed);
    } else {
        s2e_message("Accessing shared memory..");
        RUN = 2;
        region* ptr = (region*)mmap(NULL, sizeof(struct region),
                       PROT_READ, MAP_SHARED, shmfd, 0);
        if (ptr == MAP_FAILED) {
            s2e_message("mmap error");
            s2e_message_int(errno);
            assert_model(ptr != 0, "ptr != 0");
        }
        memcpy(files[fd].content, ptr->data, ptr->size);
        files[fd].size = rd = ptr->size;
        return fd;
    }

    s2e_message("read value:");
    s2e_message_int(rd);
    assert_model(rd != 0, "disk read is not empty");
    
    if (s2e_version() != 0) {
        s2e_message_int(symbFromFile);
        if (symbFromFile) {
            s2e_message("Getting position from pos.dat");
            char description[100];
            int pos, length;

            FILE *fin = fopen("pos.dat", "r");
            assert(fin);

            fscanf(fin, "%s %d %d files[fd].content:%d", description, &pos, &length, (int)files[fd].content+pos);
            make_symbolic(files[fd].content+pos, length, description);
            s2e_message("description");
            s2e_message_int(pos);
            s2e_message_int(length);

            fclose(fin);
        } else {
            for (int i = 0; i < SYMB_N; ++i) {
                make_symbolic(files[fd].content+SYMB_ADDRESS[i], SYMB_SIZE[i], SYMB_DESC[i]);
            }
        }

    }

    return fd;
}

off_t lseek(int fd, off_t offset, int whence) {
    assert_model(fd >= 0 && fd < FSIZE, "lseek:fd not in the range");
    assert_model(whence == SEEK_SET || whence == SEEK_CUR || whence == SEEK_END, "whence invalid");

#ifdef BAD_SECTORS
    if (latentError == 0) {
        make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            latentError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif

    if (whence == SEEK_SET) {
        if (offset < 0 || offset > files[fd].size) { errno = EINVAL; return -1; }
        files[fd].seek = offset;
    } else if (whence == SEEK_CUR) {
        if (files[fd].seek + offset < 0 || files[fd].seek + offset > files[fd].size) { errno = EINVAL; return -1; }
        files[fd].seek += offset;
    } else if (SEEK_END) {
        s2e_kill_state(-1, "ERROR: SEEK_END not implemented");
    } else s2e_kill_state(-1, "ERROR: whence not found.");

    return files[fd].seek;
}

ssize_t read_m(int fd, void *buf, size_t count) {
    assert_model(fd >= 0 && fd < FSIZE, "read: fd not in the range");
    assert_model(files[fd].content != 0, "read:file not open");

    int ret = count;

    s2e_message("READ_M");
#ifdef BAD_SECTORS
    if (latentError == 0) {
        make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            latentError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif

    if (files[fd].seek + count > files[fd].size) {
        count = files[fd].size - files[fd].seek;
        ret = 0;
    }

    char *vec = (char*)buf;
    //s2e_print_expression("files[fd].seek", files[fd].seek);
    //s2e_print_expression("count", count);

    memcpy(vec, &files[fd].content[files[fd].seek], count);
    //s2e_message("after memcpy");

    files[fd].seek += count;

    return ret;
}

ssize_t write_m(int fd, const void *buf, size_t count) {
    assert_model(fd >= 0 && fd < FSIZE, "write: fd not in the range");
    assert_model(files[fd].content != 0, "write:file not open");
    assert_model(files[fd].seek + count < files[fd].size, "write: not enough file space");

    s2e_message("WRITE_M");
#ifdef BAD_SECTORS
    if (latentError == 0) {
        make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            latentError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif

    if (count != 0)
        files[fd].written = 1;

    memcpy(files[fd].content + files[fd].seek, (char*)buf, count);
    files[fd].seek += count;
    return count;
}

static void save_data(const char *file, char* data, int size) {
    int fd = shm_open(file, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    assert_model(fd != -1, "fd != -1");
    s2e_message("opened shmem");

    if (ftruncate(fd, sizeof(struct region)) == -1)
        assert_model(0, "error truncating shmem");
    s2e_message("truncated");

    region* r = (region*) mmap(NULL, sizeof(struct region),
                   PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);

    if (r == MAP_FAILED)
        assert_model(0, "error mmapping");
    s2e_message("created region");

    r->size = size;
    s2e_message("memcpy. size: ");
    s2e_message_int(size);
    s2e_message("datasize: ");
    s2e_message_int(MAX_SIZE);
    memcpy(r->data, data, size);
}


char in[1024*1024];
void end_model(int diskfd, char recovered_path, int retcode) {
    //if (retcode != 100) return;
    char out[4000] = {0};
    struct file f = files[diskfd];

    // avoid wrapping up more than once (due to atexit)
    static int ended = 0;
    if (ended || !diskfd)
        return;
    else ended++;
    
    //s2e_message("Test data:");
    //for (int j = 0; j < SYMB_N; ++j) {
    //    s2e_message_int2("symb address:" , SYMB_ADDRESS[j]);
    //    s2e_message_int2("symb size:", SYMB_SIZE[j]);
    //    strcpy(out, SYMB_DESC[j]);
    //    int len = strlen(out);

    //    for (int i = 0; i < SYMB_SIZE[j]; ++i) {
    //        char byte = f.content[i + SYMB_ADDRESS[j]];
    //        //s2e_concretize(&byte, 1);
    //        char byte2 = s2e_get_example_uint(byte);
    //        //s2e_print_expression("byte", byte2);
    //        sprintf(out + len, " %x ", byte2);
    //        while (out[len])
    //            len++;
    //    }
    //    s2e_message(out);
    //}

//    if ((retcode == 4 || retcode == 8) && f.written != 0) {
//        s2e_message("ERROR: written to unrecovered file");
//    }
//
//    if (RUN == 1 && (retcode == 3 || retcode == 4 || retcode == 7)) { // first run. should save data in shared memory
        s2e_message("Saving data in shared memory..");
        save_data(SHMEM_NAME, files['/'].content, files['/'].size);
        s2e_message("after save data..");

        //write_file("diskToMount", files['/'].content, files['/'].size);

//    } else if (RUN == 2) {
//        shm_unlink(SHMEM_NAME);
//    }
}

#endif

