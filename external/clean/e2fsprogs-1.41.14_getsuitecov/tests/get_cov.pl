#!/usr/bin/perl

use strict;
use warnings;
use Cwd;

my $dir = $ARGV[0];
$dir =~ s/\/$//g;

my $wd = getcwd();
die unless chdir($dir);

#print "ls $dir/*.c\n";
my $out = `ls *.gcda`;
die unless length $out != 0;

my $total_lines = 0;
my $hit_lines = 0;

while ($out =~ /(\w+)\.gcda/g) {
    my $file = $1;
    chomp($file);
#    print "/usr/bin/gcov $file\n";
    my $out = `/usr/bin/gcov $file\n`;

#    print "out $out\n";

    my $str = ($out =~ /(\d+\.\d+)% of (\d+)/g)[-1];
    next if not defined $str;
    $str =~ /(\d+\.\d+)% of (\d+)/g;
    next if not defined $1 or not defined $2;

#print STDERR "$file: $1 out of $2\n";

    $total_lines += $2;
    $hit_lines += ($1 * $2)/100;
#print "out: $out";
#print "nums: $total_lines $hit_lines\n";
}

die unless chdir($wd);
#print "$total_lines $hit_lines ", $hit_lines / $total_lines, "\n";
if ($total_lines == 0) {
    print "0\n";
    exit;
}
print $hit_lines / $total_lines, "\n";
