#ifndef _MODELS_H_
#define _MODELS_H_

#include <errno.h>
#include <stddef.h>
#include "s2e.h"
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <assert.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdio.h>

static const int BLOCK_SIZE = 1024;

#define BLOCKS 10000
#define FSIZE 300
#define MEM_SIZE 10*1024*1024
#define MAX_SIZE (BLOCKS * 1024)
#define MAX_BADSECTOR_IO 10000
//#define BAD_SECTORS 1

int badSectorIO_index = 0;
char badSectorBit;
char latentError = 0; // have we failed so far?

static int loadFromFile = 0; // 1=get symbolic metadata from file. 0=get it from the hardcoded fields below
const int SYMB_N = 1; 
const int SYMB_ADDRESS[] = {5120};
const int SYMB_SIZE[] = {2048};
const char* SYMB_DESC[] = {"symbolic inode table"};

const char* SHMEM_NAME = "sb_shmem";

typedef struct region {
    int size;
    char data[MAX_SIZE];
}region;

typedef struct file {
    char name[40];
    unsigned int seek;
    char* content;
    unsigned int size;
    int mode;
    char written; // has fsck written to this file?
} file;

static int RUN = 0; // how many times has fsck been ran so far
char* mem;
unsigned int off = 0; // malloc offset
static int _FD = -1; // main file descriptor

file files[FSIZE] = {0};

void make_symbolic(void* data, int length, const char* description) {
    s2e_make_symbolic(data, length, description);
}

void assert_model(char b, const char* msg) {
    if (!b) {
        if (s2e_version() != 0) {
            s2e_message("ERROR model: ");
            s2e_kill_state(-2, msg);
        } else {
            puts(msg);
            assert(0);
        }
    }
}

void init_model(int bs, char lff) {
    s2e_message("Model init");
    loadFromFile = lff;
}

int open_m(const char *filename, int mode, const char* sb_file) {
    int fd = -1;
    int rd;
    int READING_DISK = 0;

    assert_model(filename[0] != 0, "filename invalid");
    assert_model(BLOCK_SIZE != 0, "BS is zero");

    s2e_message("open_m");
    s2e_message(filename);

    if (strstr(filename, "ram") != 0)
       READING_DISK = 1; // we are reading the right device

    // check if file is opened
    int i;
    for (i = 0; files[i].name[0] && strcmp(files[i].name, filename) != 0 && i < FSIZE; ++i)
        ;

    assert_model(i < FSIZE, "i < FSIZE");

    if (files[i].name[0]) { // this file is already opened. create new fd, share content
        s2e_message("File already opened");
        int j;
        for (j = i; j < FSIZE && files[j].name[0]; ++j) // find empty spot
            ;
        assert_model(j < FSIZE, "j < FSIZE");
        strcpy(files[j].name, files[i].name);
        files[j].seek = 0;
        files[j].mode = mode;
        files[j].content = files[i].content;
        files[j].size = files[i].size;
        return j;
    } else { // this file is not open. i is an empty slot. assume we are opening disk
        fd = i;
        strcpy(files[fd].name, filename);
        files[fd].seek = 0;
        files[fd].mode = mode;
    }

    if (READING_DISK == 1) {
	int shmfd = shm_open(SHMEM_NAME, O_RDONLY, S_IRUSR);
	if (shmfd == -1 && _FD == -1) { // first run. get superblock from file 'sb'
		s2e_message_int(errno);
		s2e_message("First run. Getting sb from file.");
		RUN = 1;
		int filed = open(sb_file, mode);
		s2e_assert(filed != -1, "SB read");

                files[fd].content = (char*)malloc(BLOCKS * BLOCK_SIZE); 
		rd = read(filed, files[fd].content, BLOCKS * BLOCK_SIZE);
		files[fd].size = rd;
		close(filed);

                if (s2e_version() != 0) {
                    if (loadFromFile) {
                        s2e_message("Getting sb from pos.dat.");
                        FILE* fin = fopen("pos.dat", "r");
                        char description[100];
                        int pos, length;
                        assert(fin);

                        while (fscanf(fin, "%s %d %d", description, &pos, &length) == 3) {
                            s2e_message(description);
                            s2e_message_int(pos);
                            s2e_message_int(length);
                            make_symbolic(files[fd].content+pos, length, description);
                        }
                        
                        fclose(fin);
                    } else {
			int i;
			for (i = 0; i < SYMB_N; ++i) {
				make_symbolic(files[fd].content+SYMB_ADDRESS[i], SYMB_SIZE[i], SYMB_DESC[i]);
			}
                    }
		
		}
                if (_FD == -1)
                    _FD = fd;
                return fd;
	} else {
		s2e_message("Accessing shared memory..");
		RUN = 2;
		region* ptr = (region*)mmap(NULL, sizeof(struct region),
			PROT_READ, MAP_SHARED, shmfd, 0);
                if (ptr == MAP_FAILED) {
                    s2e_message("mmap error");
                    s2e_message_int(errno);
                    assert_model(ptr != 0, "ptr != 0");
                }
                files[fd].size = 0;
                files[fd].content = (char*)malloc(ptr->size);
                assert_model(files[fd].content != 0, "Not enough content memory");
                s2e_message("region size:"); s2e_message_int(ptr->size);
                memcpy(files[fd].content, ptr->data, ptr->size);
                files[fd].size = rd = ptr->size;
               
                shm_unlink(SHMEM_NAME);
                if (_FD == -1)
                    _FD = fd;
		return fd;
	}
    } else {
       assert_model(0, "Not reading disk");
    }

    if (_FD == -1)
        _FD = fd;
    return fd;
}

void close_m() {

}

off_t lseek_m(int fd, off_t offset, int whence) {
    assert_model(fd >= 0 && fd < FSIZE, "lseek:fd not in the range");
    assert_model(whence == SEEK_SET || whence == SEEK_CUR || whence == SEEK_END, "whence invalid");

#ifdef BAD_SECTORS
    if (latentError == 0) {
        make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            latentError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif

    if (whence == SEEK_SET) {
        if (offset < 0 || offset > files[fd].size) { errno = EINVAL; return -1; }
        files[fd].seek = offset;
    } else if (whence == SEEK_CUR) {
        if (files[fd].seek + offset < 0 || files[fd].seek + offset > files[fd].size) { errno = EINVAL; return -1; }
        files[fd].seek += offset;
    } else if (SEEK_END) {
        s2e_kill_state(-1, "ERROR: SEEK_END not implemented");
    } else s2e_kill_state(-1, "ERROR: whence not found.");

    //assert_model(files[fd].seek >= 0 && files[fd].seek < files[fd].size, "incorrect seek");
    return files[fd].seek;
}

ssize_t read_m(int fd, void *buf, size_t count) {
    assert_model(fd >= 0 && fd < FSIZE, "read: fd not in the range");
    assert_model(files[fd].content != 0, "read:file not open");

    //s2e_message("READ_M");
    //s2e_message_int(count);
    //s2e_message_int(files[fd].seek);
    int ret = count;

#ifdef BAD_SECTORS
    if (latentError == 0) {
        make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            latentError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif
    if (files[fd].seek + count > files[fd].size) {
        count = files[fd].size - files[fd].seek;
        ret = 0;
    }

    char *vec = (char*)buf;
    memcpy(vec, &files[fd].content[files[fd].seek], count);
    files[fd].seek += count;

    return ret;
}

ssize_t write_m(int fd, const void *buf, size_t count) {
    //s2e_message("WRITE");
    s2e_message_int(count);
    s2e_message_int(files[fd].seek);
    assert_model(fd >= 0 && fd < FSIZE, "write: fd not in the range");
    //s2e_message("WRITE1");
    assert_model(files[fd].content != 0, "write:file not open");
    //s2e_message("WRITE2");
    assert_model(files[fd].seek + count < files[fd].size, "write: not enough file space");
    
#ifdef BAD_SECTORS
    if (latentError == 0) {
        make_symbolic(badSectorBit, 1, "badSector");
        if (badSectorBit == 1) {
            latentError = 1; // we just want to fail once per run
            errno = EIO;
            return -1;
        }
    }
#endif

    //s2e_message("WRITE3");
    assert_model(fd == _FD, "fd == _FD");
    memcpy(files[fd].content + files[fd].seek, (char*)buf, count);
    //s2e_message("WRITE4");
    files[fd].seek += count;
    return count;
}

void* malloc_m(unsigned int n) {
    assert_model(off + n < MEM_SIZE, "not enough memory");
    assert(mem);
    void* ret = &mem[off];
    off += n;
    return ret;
}

static void save_data(const char *file, char* data, int size) {
    int fd = shm_open(file, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR);
    assert_model(fd != -1, "fd != -1");
    assert_model(size < MAX_SIZE, "size < MAX_SIZE");

    if (ftruncate(fd, sizeof(struct region)) == -1)
        assert_model(0, "error truncating shmem");


    region* r = (region*) mmap(NULL, sizeof(struct region),
                   PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    

    if (r == MAP_FAILED)
        assert_model(0, "error mmapping");

    r->size = size;
    s2e_message("Shared memory size: ");
    s2e_message_int(size);
    memcpy(r->data, data, size);
}

char in[1024*1024];
void end_model(char recovered_path, int retcode) {

    assert_model(_FD != -1, "_FD != -1");

    int diskfd = _FD;
    file f = files[diskfd];

    // avoid wrapping up more than once (due to atexit)
    static int ended = 0;
    if (ended)
        return;
    else ended++;
    
    s2e_message("Test data:");
    s2e_message("retcode:");
    s2e_message_int(retcode);
skip:

    s2e_message("Saving data in shared memory.");
    //if ((retcode == 0 || retcode == 4 || retcode == 8) && f.written == 1) {
    //    s2e_message("ERROR: written to disk.");
    //}

    //if (RUN == 1 && (retcode == 1 || retcode == 2 || retcode == 4 || retcode == 5 || retcode == 6)) { // first run. should save data in shared memory
    //    s2e_message("Saving data in shared memory.");
    //    save_data(SHMEM_NAME, files[_FD].content, files[_FD].size);
    //} else if (RUN == 2 && (retcode == 1 || retcode == 2 || retcode == 5 || retcode == 6)) {
    //    s2e_message("shm_unlink");
    //    shm_unlink(SHMEM_NAME);
    //    //s2e_message("ERROR: fsck recovered second time");
    //    //s2e_message_int(retcode);
    //} else if (RUN == 2) {
    //    s2e_message("shm_unlink2");
    //    shm_unlink(SHMEM_NAME);
    //}
}



#endif

