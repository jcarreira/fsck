#include "utils.h"
#include <cassert>
#include <sstream>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <vector>
#include "xml.h"

using namespace std;

char cmd[1000];
char cwd[1000];

void alarm_handler (int i) {
    puts("pgrep qemu | xargs kill -15");
    sleep(2);
    system("pgrep qemu | xargs kill -9");
    exit(0);
}

void parse_dir(string directory) {
    if (*directory.rbegin() == '/') {
        *directory.rbegin() = 0;
    }
}

std::string exec(const char* cmd) {
    FILE* pipe = popen(cmd, "r");
    assert(pipe);
    char buffer[128];
    std::string result = "";
    while(!feof(pipe)) {
        if(fgets(buffer, 128, pipe) != NULL)
            result += buffer;
    }
    pclose(pipe);
    return result;
}

int main(int argc, char* argv[]) {
    if (argc != 5) {
        puts("launch timeout directory specification experimentName");
        assert(0);
    }

    signal(SIGALRM, alarm_handler);

    int timeout = strToInt(argv[1]);
    string directory = argv[2];
    string file = argv[3];
    string experimentName = argv[4];
    //string sb = argv[5];

    parse_dir(directory);

    // create directory to hold experiments results
    sprintf(cmd, "mkdir %s/%s", directory.c_str(), experimentName.c_str());
    puts(cmd);
    system(cmd);

    xml_open(file.c_str());
    vector<struct Field*> fields = xml_get_fields();

    assert(getcwd(cwd, sizeof(cwd)));

    char set_date[100];
    sprintf(set_date, "date > %s/%s/date_time\n", directory.c_str(), experimentName.c_str());
    system(set_date);

    for (int i = 0; i < (int)fields.size(); ++i) {
        int position = xml_get_field_pos(fields[i]);
        string name = xml_get_field_name(fields[i]);
        int size = xml_get_field_size(fields[i]);
        PointerType fieldType(xml_get_field_type(fields[i]));

        printf("Testing field %d. name: %s  position: %d  size: %d\n", i, name.c_str(), position, size);

        // copy pos.dat to /tests
        sprintf(cmd, "echo \"%s %d %d %d\" > pos.dat; cp pos.dat %s/tests/pos.dat", name.c_str(), position, size, fieldType, directory.c_str());
        puts(cmd);
        system(cmd);

        // go to builds directory
        assert(chdir(directory.c_str()) == 0);
        system("date > testdate_time");


        int pid = fork();
        if (pid == 0) { // child

            alarm(timeout);
            // launch s2e
            puts("./launchvm.sh ready -nographic -s2e-max-processes 8");
            system("./launchvm.sh ready -nographic -s2e-max-processes 8");

            while (1) {
                string out = exec("ps aux |grep qemu-release| grep -v grep |wc -l");
                int qemus = strToInt(out);
                printf("Qemus: %d\n", qemus);
                if (!qemus) break; // there are no more qemus. execution is over 
                sleep(2);
            }

            puts("S2E Execution done!!");
            system("pgrep qemu | xargs kill -15");
            sleep(2);
            system("pgrep qemu | xargs kill -9");
            exit(0);
        } else {
            sleep(30); //sleep 1 minute
            int isStalled = system("./isStalled.pl s2e-last");
            isStalled = WEXITSTATUS(isStalled);
            // if it is stalled
            // kill s2e
            // and restart
            if (isStalled == 0) {
                puts("S2E is stalled. Restarting..");
                kill(pid, SIGKILL);
                system("pgrep qemu | xargs kill -9");
                system("pgrep qemu | xargs kill -9");
                sleep(2);
                i--;
                assert(chdir(cwd) == 0);
                continue;
            }

            int ret;
            // set alarm
            puts("Waiting for child");
            wait(&ret);
            system("pgrep qemu | xargs kill -15");
            sleep(2);
            system("pgrep qemu | xargs kill -9");
        }

        system("date >> testdate_time");
        system("mv testdate_time s2e-last");
        // copy results to separate folder
        sprintf(cmd, "cp -L s2e-last %s/test_%s_%d -r", experimentName.c_str(), name.c_str(), position);
        puts(cmd);
        system(cmd);
        sleep(5);

//           system("cp build_minix/hd_img.qcow2 build_ext2/hd_img.qcow2");

        assert(chdir(cwd) == 0);
    }
        sprintf(set_date, "date >> %s/%s/date_time", directory.c_str(), experimentName.c_str());
        system(set_date);
}

