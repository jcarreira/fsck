#include "rapidxml.hpp"
#include "utils.h"
#include <fstream>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include "xml.h"

using namespace std;
using namespace rapidxml;

static void parse_file(const char* file);
char str[100000];

string spaceForUnderscores(string s) {
    for (int i = 0; i < (int)s.size(); ++i) {
        if (s[i] == ' ')
            s[i] = '_';
    }
    return s;
}

//enum PointerType {
//    Unknown = 0,
//    BlockPointer = 1,
//    InodePointer = 2,
//};

PointerType fieldTypeToEnum(string type) {
    map<string, PointerType> map;
    map["blockPointer"] = BlockPointer;
    map["inodePointer"] = InodePointer;

    cout << "Type: " << type << endl;
    if (map.find(type) == map.end())
        return Unknown;

    return map[type];
}

struct Field {
    string name;
    int position;
    int size;
    PointerType pointerType;
    bool badSectorEnabled;
    bool dataIntegrity;
    bool dataSecurity;
    
    Field(string str, int pos, int s, PointerType ptrType = Unknown, bool badSector = 0, bool dataInt = 0, bool dataSec = 0) :
        name(spaceForUnderscores(str)),
        position(pos),
        size(s),
        pointerType(ptrType),
        badSectorEnabled(badSector),
        dataIntegrity(dataInt),
        dataSecurity(dataSec)
    {
    }

    Field() {}

    Field(const Field& f) :
        name(f.name),
        position(f.position),
        size(f.size),
        badSectorEnabled(f.badSectorEnabled),
        dataIntegrity(f.dataIntegrity),
        dataSecurity(f.dataSecurity)
    {}
};

struct Cmp {
	bool operator()(const Field* f1, const Field* f2) {
		return f1->position < f2->position;
	}
} myCmp;

ostream& operator<<(ostream& out, const Field& f) {
//    out << "Field name: " << f.name << " pos: " << f.position << " size: " << f.size;
    out << "Field name: " << f.name << " pos: " << f.position << " size: " << f.size << " " << f.size << 
        " fieldType: " << f.pointerType << " badSector: " << f.badSectorEnabled << " dataIntegrity: " << f.dataIntegrity << " dataSecurity: " << "\n";
    return out;
}

class Group {
public:
    string name; // name of group (e.g., Superblock)
    map<string, struct Field> fieldsMap; // map to get fields by name

    Group(string str) {
        name = str;
    }

    Group(){ }

    void add(const Field& f) {
        fieldsMap[f.name] = f;
    }

    map<string, struct Field>& getFields() {
        return fieldsMap;
    }
};

map<string, Group> groups;
vector<Field*> fields;
vector<Field*> dataIntegrityFields;
vector<Field*> dataSecurityFields;


template<class C>
void check(C n, string msg) {
    if (!n) {
        cout << msg << std::endl;
        exit(-1);
    }
}

void processTest(const string& type, int startPosition, const string& field) {
    check(groups.find(type) != groups.end(), "Structure unknow: " + type);
    map<string, struct Field> fieldsMap = groups[type].getFields();
    //cout << type << startPosition << field << std::endl;
    if (field != "") { // if we are testing a specific field
        //cout << "Test field " << std::endl;
        assert(fieldsMap.find(field) != fieldsMap.end());
        Field f = fieldsMap[field];

        f.position += startPosition;
        Field* new_field = new Field(f);
        fields.push_back(new_field);

        if (f.dataIntegrity)
            dataIntegrityFields.push_back(new Field(f));
        if (f.dataSecurity)
            dataSecurityFields.push_back(new Field(f));

    } else { // go through all fields
        //cout << "Running test for struct: " << type << std::endl;
        check(fieldsMap.size() != 0, "Structure does not have fields:" + type);
        for (map<string, struct Field>::iterator it = fieldsMap.begin(); it != fieldsMap.end(); ++it) {
            string fieldName = it->first;
            Field f = fieldsMap[fieldName];

            f.position += startPosition;
            Field* new_field = new Field(f);
            fields.push_back(new_field);

            if (f.dataIntegrity)
                dataIntegrityFields.push_back(new Field(f));
            if (f.dataSecurity)
                dataSecurityFields.push_back(new Field(f));
            cout << "Testing field: " << f; 
        }
    }
}

void xml_open(string file) {
    parse_file(file.c_str());
}

static void parse_file(const char* file) {
    FILE* fin = fopen(file, "r");
    assert(fin);
    fread(str, sizeof(str), 1, fin);

    xml_document<> doc;
    doc.parse<0>(str);

    xml_node<>* fsNode = doc.first_node();
    assert(fsNode);
    string rootNodeName(fsNode->name());
    string filesystemName = fsNode->first_attribute("name")->value();

    assert(rootNodeName == "FileSystem");
    assert(filesystemName == "Ext2" || filesystemName == "Minix" || filesystemName == "Ext4" || filesystemName == "ReiserFS");

    xml_node<>* node = doc.first_node()->first_node();

    while (node) {
        string nodeName = node->name();
        xml_attribute<>* nodeAttribute = node->first_attribute("name");
        string nodeType = (nodeAttribute ? nodeAttribute->value() : "");
        //cout << "Node under <FileSystem> has name " << nodeName << "\n";
        if (nodeName == "AddStructure") {
            string name = node->first_attribute("name")->value();
            //cout << "<AddStructure> has type " << name << "\n";
            Group group(name); // create group

            xml_node<>* field = node->first_node();
            while (field) {
                //cout << field->name() << std::endl;
                assert(string(field->name()) == "Field");

                string name = field->first_attribute("name")->value();
                string position = field->first_attribute("position")->value();
                string size = field->first_attribute("size")->value();
                xml_attribute<>* type = field->first_attribute("type");
                string fieldType = (type ? type->value() : "");
               
                // get testTypes: dataIntegrity, dataSecurity 
                bool badSectorEnabled;
                bool dataIntegrity;
                bool dataSecurity;
                for (xml_attribute<>* type = field->first_attribute("testType"); type; type = type->next_attribute()) {
                    string fieldType = type->value();
                    if (fieldType == "badSector") {
                        badSectorEnabled = 1;
                    } else if (fieldType == "dataIntegrity") {
                        dataIntegrity = 1;
                    } else if (fieldType == "dataSecurity") {
                        dataSecurity = 1;
                    } else assert(0);
                }
                
                Field f(name, strToInt(position), strToInt(size), fieldTypeToEnum(fieldType), badSectorEnabled, dataIntegrity, dataSecurity);
                group.add(f);

                cout << "Field under " << nodeType <<" has name " << name << " position: " << position << " size: " << size << " " << strToInt(size) << 
                    " fieldType: " << fieldTypeToEnum(fieldType) << " badSector: " << badSectorEnabled << " dataIntegrity: " << dataIntegrity << " dataSecurity: " << "\n";
                cout << group.getFields()[name] << std::endl;
                field = field->next_sibling();
            }
            groups[name]= group;
        } else break; // go for <Test>

        node = node->next_sibling();
    }

    
    // handle tests
    node = fsNode->first_node("AddTest");
    check(node, "Tests not present");

    xml_node<>* tests = node->first_node();

    while (tests) {
        string name;
        string type;
        string startPosition;
        string field;

        name = tests->name();
        // type is the name of the structure
        type = tests->first_attribute("name")->value();
        startPosition = tests->first_attribute("startPosition")->value();
        xml_attribute<>* fieldNode = tests->first_attribute("field");
        field = (fieldNode ? fieldNode->value() : "");

        //cout << name << " type: " << type << " startPosition: " << startPosition << " field: " << field << std::endl;

        // test structure
        // type: name of previously defined structure
        // startPosition: place where structure exists in disk
        // field: in case we just want to test one field of a structure
        processTest(type, strToInt(startPosition), field);
        tests = tests->next_sibling();
    }
    sort(fields.begin(), fields.end(), myCmp);
}

int xml_get_field_pos(const Field* f) {
    return f->position;
}

const char* xml_get_field_name(const Field* f) {
    return f->name.c_str();
}

int xml_get_field_size(const Field* f) {
    return f->size;
}


vector<Field*> xml_get_fields() {
    sort(fields.begin(), fields.end(), myCmp);
    return fields;
}

PointerType xml_get_field_type(const Field* f) {
    return f->pointerType;
}

std::vector<struct Field*> xml_get_dataintegrity_fields() {
    return dataIntegrityFields;
}

std::vector<struct Field*> xml_get_datasecurity_fields() {
    return dataSecurityFields;
}

