#include <cstring>
#include <sstream>
#include <cassert>
#include <algorithm>

using namespace std;

static int strToInt(string str) {
    int ret;

    assert(str.size());
    if (str[str.size() -1] == 'h') { // hexadecimal number
        int n;
        str[str.size()-1] = 0; str = "0x" + str;
        sscanf(str.c_str(), "%x", &n);
        return n;
    }
    
    stringstream ss(str);
    ss >> ret;
    return ret;
}
