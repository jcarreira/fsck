#ifndef _XMLL_H_
#define _XMLL_H_

#include <string>
#include <vector>

struct Field;

enum PointerType {
    Unknown = 0,
    BlockPointer = 1,
    InodePointer = 2,
};

void xml_open(std::string file);
std::vector<struct Field*> xml_get_fields();
std::vector<struct Field*> xml_get_dataintegrity_fields();
std::vector<struct Field*> xml_get_datasecurity_fields();

int xml_get_field_pos(const struct Field*);
const char* xml_get_field_name(const struct Field*);
int xml_get_field_size(const struct Field*);
PointerType xml_get_field_type(const struct Field* f);
#endif
