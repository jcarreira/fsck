CXX = g++
CFLAGS = -Wall -ggdb 

all: comp check_mountability


comp:comp.cpp hierarchy.cpp
	$(CXX) $(CFLAGS) $^ -o $@

check_mountability:check_mountability.cpp
	$(CXX) $(CFLAGS) $^ -o $@

clean:
	rm *~ comp -rf
