#!/bin/sh
# script to create base file system for testing

mkdir minix_fs
mount -o loop -O user_xattr -t minix minix_imagev2 minix_fs

# create dir 'd' in '/'
mkdir minix_fs/d

# create file to fill with data and set attributes
touch minix_fs/f
# make sure we have 7 direct pointers, one indirect pointer and one double indirect pointer
dd if=zeros of=minix_fs/f bs=1024 count=278

# create special files (fifo, char dev, block dev)
mknod minix_fs/fifo_dev p
mknod minix_fs/char_dev c 1 1
mknod minix_fs/block_dev b 8 5

#create symlink and hardlink
ln -s ../ minix_fs/d/symlink
ln minix_fs/f minix_fs/d/hardlink

# create d2 inside dir
mkdir minix_fs/d/d2

# create another file with 2 blocks
touch minix_fs/f2
dd if=zeros of=minix_fs/f2 bs=1 count=2048
#chown jc1 ext4_fs/f2

for i in `seq 0 100`; do touch minix_fs/d/d2/f$i; done
