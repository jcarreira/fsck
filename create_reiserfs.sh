#!/bin/sh

# mkfs.reiserfs -f -s 513 -b 512 /dev/ram5 700
# script to create base file system for testing


mkdir reiserfs_fs
mount -t reiserfs -o loop -O user_xattr  reiserfs_image reiserfs_fs

# create dir 'd' in '/'
mkdir reiserfs_fs/d

# create file to fill with data and set attributes
touch f
dd if=zeros of=f bs=1024 count=278
setfattr -n user.teste -v "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789" f
mv f reiserfs_fs/f

# create special files (fifo, char dev, block dev)
mknod reiserfs_fs/fifo_dev p
mknod reiserfs_fs/char_dev c 1 1
mknod reiserfs_fs/block_dev b 8 5

#create symlink and hardlink
ln -s ../ reiserfs_fs/d/symlink
ln reiserfs_fs/f reiserfs_fs/d/hardlink

# create d2 inside dir
mkdir reiserfs_fs/d/d2

# create another file with 2 blocks
touch reiserfs_fs/f2
dd if=zeros of=reiserfs_fs/f2 bs=1 count=2048
chown jcar reiserfs_fs/f2

for i in `seq 0 100`; do touch ext4_fs/d/d2/f$i; done