#ifndef S2E_PLUGINS_RECOVSEARCHER_H
#define S2E_PLUGINS_RECOVSEARCHER_H

#include <s2e/Plugin.h>
#include <s2e/Plugins/CorePlugin.h>
#include <s2e/Plugins/ModuleExecutionDetector.h>
#include <s2e/S2EExecutionState.h>

#include <klee/Searcher.h>
#include <klee/ExecutionState.h>

#include <vector>

namespace s2e {
namespace plugins {

#define RECOVSEARCHER_OPCODE 0xAC

using namespace klee;

class RecoverySearcher : public Plugin, public klee::Searcher
{
    S2E_PLUGIN
public:
    /*enum CoopSchedulerOpcodes {
        ScheduleNext = 0,
        Yield = 1,
    };*/

    typedef std::map<uint32_t, S2EExecutionState*> States;

    RecoverySearcher(S2E* s2e): Plugin(s2e) {}
    void initialize();

    // the following two take care of batching time
    virtual klee::ExecutionState& selectState();
    virtual void update(klee::ExecutionState *current,
                        const std::set<klee::ExecutionState*> &addedStates,
                        const std::set<klee::ExecutionState*> &removedStates);
    virtual bool empty();

private:
    Searcher *weightedSearcher;

    bool m_searcherInited;
    States m_states;
    S2EExecutionState *m_currentState;
    
    double timeBudget;
    ExecutionState *lastState;
    double lastStartTime;

    void initializeSearcher();

    void onCustomInstruction(S2EExecutionState* state, uint64_t opcode);
};



} // namespace plugins
} // namespace s2e

#endif
