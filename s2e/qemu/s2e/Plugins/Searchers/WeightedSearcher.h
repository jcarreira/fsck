#ifndef WEIGHTEDSEARCHER_H
#define WEIGHTEDSEARCHER_H

#include <s2e/Plugin.h>
#include <s2e/Plugins/CorePlugin.h>
#include <s2e/Plugins/ModuleExecutionDetector.h>
#include <s2e/S2EExecutionState.h>

#include <klee/Searcher.h>
#include <klee/ExecutionState.h>
#include <klee/Internal/ADT/DiscretePDF.h>
#include <klee/Internal/ADT/RNG.h>

#include <vector>

namespace s2e {
namespace plugins {

using namespace klee;

#define WEIGHTSEARCHER_OPCODE 0x60

//namespace klee {
//  extern RNG theRNG;
//}

class WeightedSearcher : public Plugin, public Searcher
{

    S2E_PLUGIN
public:
    DiscretePDF<ExecutionState*> *states;

    WeightedSearcher(S2E* s2e): Plugin(s2e) { states = new DiscretePDF<ExecutionState*>(); s2e->getCorePlugin()->onCustomInstruction.connect(sigc::mem_fun(*this, &WeightedSearcher::onCustomInstruction)); }
    void initialize();

    virtual ExecutionState& selectState() { RNG rng; return *states->choose(rng.getDoubleL()); }


    virtual void update(ExecutionState *current,
                        const std::set<ExecutionState*> &addedStates,
                        const std::set<ExecutionState*> &removedStates);

    virtual bool empty();

private:
    bool m_searcherInited;

    double getWeight(ExecutionState*);
    void initializeSearcher();
    void onCustomInstruction(S2EExecutionState* state, uint64_t opcode);
};



} // namespace plugins
} // namespace s2e

#endif
