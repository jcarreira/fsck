extern "C" {
#include "config.h"
#include "qemu-common.h"
}

#include <s2e/S2E.h>
#include <s2e/ConfigFile.h>
#include <s2e/Utils.h>
#include <s2e/S2EExecutor.h>

#include <cstdio>
#include <iostream>

#include "WeightedSearcher.h"

namespace s2e {
namespace plugins {

using namespace llvm;

S2E_DEFINE_PLUGIN(WeightedSearcher, "Uses custom instructions to schedule states",
                  "WeightedSearcher");

void WeightedSearcher::initialize()
{
    //m_searcherInited = false;
    //initializeSearcher();
}

void WeightedSearcher::initializeSearcher()
{

}


void WeightedSearcher::update(klee::ExecutionState *current,
                    const std::set<klee::ExecutionState*> &addedStates,
                    const std::set<klee::ExecutionState*> &removedStates)
{
  for (std::set<ExecutionState*>::const_iterator it = addedStates.begin(),
         ie = addedStates.end(); it != ie; ++it) {
      ExecutionState *es = *it;
      //printf("add state: %u\n", es);
      states->insert(es, getWeight(es));
  }

  if (current && !removedStates.count(current) && states->inTree(current)) {
      //printf("update state: %u\n", current);
    states->update(current, getWeight(current));
  }
//  
  for (std::set<ExecutionState*>::const_iterator it = removedStates.begin(),
         ie = removedStates.end(); it != ie; ++it) {
    states->remove(*it);
  }
}
#define MIN(a,b) ((a) < (b) ? (a) : (b))
double WeightedSearcher::getWeight(ExecutionState *es) {
   //int recoveries = es->getNumRecoveries();
   //int secondRecovery = es->getSecRecovery();

   int phase = es->getPhase();
   int prio =  es->getPriority();
   double min_val = 1.0;

   if (phase == 1) { // second run
       min_val = 2;
   }

   //return MIN(min_val, 0.1*prio);
   return 0.1 * prio;
}

bool WeightedSearcher::empty()
{
    return states->empty(); 
}

void WeightedSearcher::onCustomInstruction(S2EExecutionState* state, uint64_t opcode)
{
    //XXX: find a better way of allocating custom opcodes
    if (((opcode>>8) & 0xFF) != WEIGHTSEARCHER_OPCODE) {
        return;
    }

    opcode >>= 16;
    //uint8_t op = opcode & 0xFF;

    state->setPriority(state->getPriority() + 1);

}


} // namespace plugins
} // namespace s2e
