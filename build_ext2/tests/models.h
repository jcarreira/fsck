#ifndef _MODELS_H_
#define _MODELS_H_

#include <unistd.h>

void init_model(int bs);
int open_m(const char *filename, int mode, const char* sb_file);
void close_m();
off_t lseek_m(int fd, off_t offset, int whence);
ssize_t read_m(int fd, void *buf, size_t count);
ssize_t write_m(int fd, const void *buf, size_t count);
void* malloc_m(unsigned int n);
static void save_data(const char *file, char* data, int size);
void end_model(char recovered_path, int retcode);

#endif

