#include <cstdio>
#include <stdlib.h>
#include <sys/wait.h>
#include "s2e.h"

const char* getfsck = "./s2eget Z";
const char* mvfsck  = "mv Z fsck.minix";

void progress(const char* s) { puts(s); s2e_message(s); }

int main() {
    int vec[100];
    progress("SYMB_ADDRESS");

    s2e_enable_forking();

    int ind;
    s2e_make_symbolic(&ind, sizeof(int), "ind");
    s2e_assert(ind <100, "ind<100");

    vec[ind]=1;

    for (int i = 0; i < 100; i++) {
        s2e_print_expression("vec[i]", vec[i]);
    }

    
    s2e_disable_forking();
    //s2e_enable_all_apic_interrupts();
    s2e_kill_state(0, "TERMINATED");

    return 0;
}

